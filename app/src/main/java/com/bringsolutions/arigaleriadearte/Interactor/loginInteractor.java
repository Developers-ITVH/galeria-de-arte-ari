package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class loginInteractor implements Login.Interactor
{
    Login.Presenter presenter;
    Api apiService = RetrofitInstance.getApiService();

    public loginInteractor(Login.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void Login(Usuario usuario)
    {
      Call<Usuario> usuarioCall = apiService.loginUser(usuario);
      usuarioCall.enqueue(new Callback<Usuario>()
      {
          @Override
          public void onResponse(Call<Usuario> call, Response<Usuario> response)
          {
              if (response.isSuccessful())
              {
                  if (response.body().getUsu_clave()==null)
                  {
                      presenter.ResultError(new Respuesta("Algún dato incorrecto"));
                  }else
                      {
                          presenter.ResultPresenter(response.body());
                      }

              }else
                  {
                      presenter.ResultError(new Respuesta("Teléfono o contraseña incorrecto"));
                  }

          }

          @Override
          public void onFailure(Call<Usuario> call, Throwable t)
          {
              presenter.ResultError(new Respuesta(t.getLocalizedMessage()));
          }
      });

    }
}
