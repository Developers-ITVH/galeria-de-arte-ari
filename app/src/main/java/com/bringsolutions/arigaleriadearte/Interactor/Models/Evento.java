package com.bringsolutions.arigaleriadearte.Interactor.Models;

public class Evento
{
    int eve_id;
    String eve_nombre;
    String eve_descripcion;
    String eve_fecha;
    String eve_horario;

    public Evento()
    {

    }

    public Evento(int eve_id, String eve_nombre, String eve_descripcion, String eve_fecha, String eve_horario)
    {
        this.eve_id = eve_id;
        this.eve_nombre = eve_nombre;
        this.eve_descripcion = eve_descripcion;
        this.eve_fecha = eve_fecha;
        this.eve_horario = eve_horario;
    }

    public int getEve_id()
    {
        return eve_id;
    }

    public void setEve_id(int eve_id)
    {
        this.eve_id = eve_id;
    }

    public String getEve_nombre()
    {
        return eve_nombre;
    }

    public void setEve_nombre(String eve_nombre)
    {
        this.eve_nombre = eve_nombre;
    }

    public String getEve_descripcion()
    {
        return eve_descripcion;
    }

    public void setEve_descripcion(String eve_descripcion)
    {
        this.eve_descripcion = eve_descripcion;
    }

    public String getEve_fecha()
    {
        return eve_fecha;
    }

    public void setEve_fecha(String eve_fecha)
    {
        this.eve_fecha = eve_fecha;
    }

    public String getEve_horario()
    {
        return eve_horario;
    }

    public void setEve_horario(String eve_horario)
    {
        this.eve_horario = eve_horario;
    }

    @Override
    public String toString()
    {
        return "Evento{" +
                "eve_id=" + eve_id +
                ", eve_nombre='" + eve_nombre + '\'' +
                ", eve_descripcion='" + eve_descripcion + '\'' +
                ", eve_fecha='" + eve_fecha + '\'' +
                ", eve_horario='" + eve_horario + '\'' +
                '}';
    }
}
