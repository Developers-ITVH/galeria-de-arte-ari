package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Autores;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutoresInteractor implements Autores.Interactor
{

    Autores.Presenter presenter;
    Api api = RetrofitInstance.getApiService();

    public AutoresInteractor(Autores.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void cargarAutores()
    {
        Call<List<Autor>> listCall = api.getAutores();
        listCall.enqueue(new Callback<List<Autor>>()
        {
            @Override
            public void onResponse(Call<List<Autor>> call, Response<List<Autor>> response)
            {
                if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        presenter.error(new Respuesta("No se encontraron datos"));
                    }else
                    {
                        presenter.resultAutores(response.body());
                    }
                }else
                    {
                        presenter.error(new Respuesta(response.message()));
                    }
            }

            @Override
            public void onFailure(Call<List<Autor>> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }
}
