package com.bringsolutions.arigaleriadearte.Interactor.Models;

import java.io.Serializable;

public class Escultura implements Serializable
{
    private int id;
    String esc_clave ;
    String esc_clave_autor;
    String esc_nombre;
    String esc_descripcion;
    String esc_material;
    String esc_precio;
    String esc_qr;
    String esc_anio;
    String esc_ancho;
    String esc_alto;
    String esc_estado;
    String esc_clave_remodelacion;
    String esc_foto;
    String aut_nombre;
    String aut_apellidos;

    public Escultura()
    {
    }


    public Escultura(int id, String esc_clave, String esc_clave_autor, String esc_nombre, String esc_descripcion, String esc_material, String esc_precio, String esc_qr, String esc_anio, String esc_ancho, String esc_alto, String esc_estado, String esc_clave_remodelacion, String esc_foto, String aut_nombre, String aut_apellidos)
    {
        this.id = id;
        this.esc_clave = esc_clave;
        this.esc_clave_autor = esc_clave_autor;
        this.esc_nombre = esc_nombre;
        this.esc_descripcion = esc_descripcion;
        this.esc_material = esc_material;
        this.esc_precio = esc_precio;
        this.esc_qr = esc_qr;
        this.esc_anio = esc_anio;
        this.esc_ancho = esc_ancho;
        this.esc_alto = esc_alto;
        this.esc_estado = esc_estado;
        this.esc_clave_remodelacion = esc_clave_remodelacion;
        this.esc_foto = esc_foto;
        this.aut_nombre = aut_nombre;
        this.aut_apellidos = aut_apellidos;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getEsc_clave()
    {
        return esc_clave;
    }

    public void setEsc_clave(String esc_clave)
    {
        this.esc_clave = esc_clave;
    }

    public String getEsc_clave_autor()
    {
        return esc_clave_autor;
    }

    public void setEsc_clave_autor(String esc_clave_autor)
    {
        this.esc_clave_autor = esc_clave_autor;
    }

    public String getEsc_nombre()
    {
        return esc_nombre;
    }

    public void setEsc_nombre(String esc_nombre)
    {
        this.esc_nombre = esc_nombre;
    }

    public String getEsc_descripcion()
    {
        return esc_descripcion;
    }

    public void setEsc_descripcion(String esc_descripcion)
    {
        this.esc_descripcion = esc_descripcion;
    }

    public String getEsc_material()
    {
        return esc_material;
    }

    public void setEsc_material(String esc_material)
    {
        this.esc_material = esc_material;
    }

    public String getEsc_precio()
    {
        return esc_precio;
    }

    public void setEsc_precio(String esc_precio)
    {
        this.esc_precio = esc_precio;
    }

    public String getEsc_qr()
    {
        return esc_qr;
    }

    public void setEsc_qr(String esc_qr)
    {
        this.esc_qr = esc_qr;
    }

    public String getEsc_anio()
    {
        return esc_anio;
    }

    public void setEsc_anio(String esc_anio)
    {
        this.esc_anio = esc_anio;
    }

    public String getEsc_ancho()
    {
        return esc_ancho;
    }

    public void setEsc_ancho(String esc_ancho)
    {
        this.esc_ancho = esc_ancho;
    }

    public String getEsc_alto()
    {
        return esc_alto;
    }

    public void setEsc_alto(String esc_alto)
    {
        this.esc_alto = esc_alto;
    }

    public String getEsc_estado()
    {
        return esc_estado;
    }

    public void setEsc_estado(String esc_estado)
    {
        this.esc_estado = esc_estado;
    }

    public String getEsc_clave_remodelacion()
    {
        return esc_clave_remodelacion;
    }

    public void setEsc_clave_remodelacion(String esc_clave_remodelacion)
    {
        this.esc_clave_remodelacion = esc_clave_remodelacion;
    }

    public String getEsc_foto()
    {
        return esc_foto;
    }

    public void setEsc_foto(String esc_foto)
    {
        this.esc_foto = esc_foto;
    }

    public String getAut_nombre()
    {
        return aut_nombre;
    }

    public void setAut_nombre(String aut_nombre)
    {
        this.aut_nombre = aut_nombre;
    }

    public String getAut_apellidos()
    {
        return aut_apellidos;
    }

    public void setAut_apellidos(String aut_apellidos)
    {
        this.aut_apellidos = aut_apellidos;
    }

    @Override
    public String toString()
    {
        return "Escultura{" +
                "id=" + id +
                ", esc_clave='" + esc_clave + '\'' +
                ", esc_clave_autor='" + esc_clave_autor + '\'' +
                ", esc_nombre='" + esc_nombre + '\'' +
                ", esc_descripcion='" + esc_descripcion + '\'' +
                ", esc_material='" + esc_material + '\'' +
                ", esc_precio='" + esc_precio + '\'' +
                ", esc_qr='" + esc_qr + '\'' +
                ", esc_anio='" + esc_anio + '\'' +
                ", esc_ancho='" + esc_ancho + '\'' +
                ", esc_alto='" + esc_alto + '\'' +
                ", esc_estado='" + esc_estado + '\'' +
                ", esc_clave_remodelacion='" + esc_clave_remodelacion + '\'' +
                ", esc_foto='" + esc_foto + '\'' +
                ", aut_nombre='" + aut_nombre + '\'' +
                ", aut_apellidos='" + aut_apellidos + '\'' +
                '}';
    }
}
