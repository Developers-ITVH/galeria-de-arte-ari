package com.bringsolutions.arigaleriadearte.Interactor.Models;

public class Respuesta
{
    String Respuesta;

    public Respuesta(String respuesta)
    {
        Respuesta = respuesta;
    }

    public String getRespuesta()
    {
        return Respuesta;
    }

    public void setRespuesta(String respuesta)
    {
        Respuesta = respuesta;
    }
}
