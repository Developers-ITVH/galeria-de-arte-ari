package com.bringsolutions.arigaleriadearte.Interactor;

import android.util.Log;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.Registro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroInteractor implements Registro.Interactor
{

    public String TAG = "REGISTRO";
    Registro.Presenter presenter;
    Api api = RetrofitInstance.getApiService();

    public RegistroInteractor(Registro.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void Registro(final Usuario usuario)
    {
        Call<Usuario> usuarioCall = api.registroUser(usuario);
        usuarioCall.enqueue(new Callback<Usuario>()
        {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response)
            {
                if (response.isSuccessful())
                {
                    presenter.ResultPresenter(response.body());
                }else
                    {
                presenter.ResultError(new Respuesta("Se produjo un problema al registrar, intenta más tarde."));
            }


            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t)
            {
                presenter.ResultError(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }
}
