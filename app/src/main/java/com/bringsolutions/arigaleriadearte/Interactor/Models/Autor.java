package com.bringsolutions.arigaleriadearte.Interactor.Models;

import java.io.Serializable;

public class Autor implements Serializable

{
    int aut_id;
    String aut_clave;
    String aut_nombre;
    String aut_apellidos;
    String aut_foto;
    String aut_templanza;

    public Autor()
    {

    }

    public Autor(String aut_nombre, String aut_apellidos, String aut_foto)
    {
        this.aut_nombre = aut_nombre;
        this.aut_apellidos = aut_apellidos;
        this.aut_foto = aut_foto;
    }

    public Autor(int aut_id, String aut_clave, String aut_nombre, String aut_apellidos, String aut_foto, String aut_templanza) {
        this.aut_id = aut_id;
        this.aut_clave = aut_clave;
        this.aut_nombre = aut_nombre;
        this.aut_apellidos = aut_apellidos;
        this.aut_foto = aut_foto;
        this.aut_templanza = aut_templanza;
    }

    public int getAut_id()
    {
        return aut_id;
    }

    public void setAut_id(int aut_id)
    {
        this.aut_id = aut_id;
    }

    public String getAut_clave()
    {
        return aut_clave;
    }

    public void setAut_clave(String aut_clave)
    {
        this.aut_clave = aut_clave;
    }

    public String getAut_nombre()
    {
        return aut_nombre;
    }

    public void setAut_nombre(String aut_nombre)
    {
        this.aut_nombre = aut_nombre;
    }

    public String getAut_apellidos()
    {
        return aut_apellidos;
    }

    public void setAut_apellidos(String aut_apellidos)
    {
        this.aut_apellidos = aut_apellidos;
    }

    public String getAut_foto()
    {
        return aut_foto;
    }

    public void setAut_foto(String aut_foto)
    {
        this.aut_foto = aut_foto;
    }

    public String getAut_templanza() {
        return aut_templanza;
    }

    public void setAut_templanza(String aut_templanza) {
        this.aut_templanza = aut_templanza;
    }

    @Override
    public String toString() {
        return "Autor{" +
                "aut_id=" + aut_id +
                ", aut_clave='" + aut_clave + '\'' +
                ", aut_nombre='" + aut_nombre + '\'' +
                ", aut_apellidos='" + aut_apellidos + '\'' +
                ", aut_foto='" + aut_foto + '\'' +
                ", aut_templanza='" + aut_templanza + '\'' +
                '}';
    }
}
