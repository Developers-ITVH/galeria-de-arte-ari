package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Obras_Esculturas;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObrasEsculturasInteractor implements Obras_Esculturas.Interactor
{
    Api api = RetrofitInstance.getApiService();
    Obras_Esculturas.Presenter presenter;

    public ObrasEsculturasInteractor(Obras_Esculturas.Presenter presenter)
    {
        this.presenter = presenter;
    }


    @Override
    public void consultarObras()
    {
        Call<List<Obra>> listCall = api.getObras();
        listCall.enqueue(new Callback<List<Obra>>()
        {
            @Override
            public void onResponse(Call<List<Obra>> call, Response<List<Obra>> response)
            {
                if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        presenter.error(new Respuesta(""));
                    }else
                        {
                            presenter.resultObras(response.body());
                        }
                }else
                    {
                        presenter.error(new Respuesta("No se han encontrado esculturas"));
                    }
            }

            @Override
            public void onFailure(Call<List<Obra>> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }

    @Override
    public void consultarEsculturas()
    {
        Call<List<Escultura>> listCall = api.getEsculturas();
        listCall.enqueue(new Callback<List<Escultura>>()
        {
            @Override
            public void onResponse(Call<List<Escultura>> call, Response<List<Escultura>> response)
            {
                if (response.isSuccessful())
                {
                    presenter.resultEsculturas(response.body());
                }else
                    {
                        presenter.error(new Respuesta("Error al traer las esculturas"));
                    }
            }

            @Override
            public void onFailure(Call<List<Escultura>> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }
}
