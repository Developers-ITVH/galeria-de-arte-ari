package com.bringsolutions.arigaleriadearte.Interactor.Models;

public class Usuario
{

    int id ;
    String usu_clave;
    String usu_nombre;
    String usu_telefono;
    String usu_email;
    String usu_pass;
    String usu_appaterno;
    String usu_apmaterno;
    String usu_tipo;


    public Usuario()
    {

    }

    public Usuario(String usu_clave, String usu_nombre, String usu_telefono, String usu_email, String usu_pass, String usu_appaterno, String usu_apmaterno, String usu_tipo)
    {
        this.usu_clave = usu_clave;
        this.usu_nombre = usu_nombre;
        this.usu_telefono = usu_telefono;
        this.usu_email = usu_email;
        this.usu_pass = usu_pass;
        this.usu_appaterno = usu_appaterno;
        this.usu_apmaterno = usu_apmaterno;
        this.usu_tipo = usu_tipo;
    }

    public String getUsu_clave()
    {
        return usu_clave;
    }

    public void setUsu_clave(String usu_clave)
    {
        this.usu_clave = usu_clave;
    }

    public String getUsu_telefono()
    {
        return usu_telefono;
    }

    public void setUsu_telefono(String usu_telefono)
    {
        this.usu_telefono = usu_telefono;
    }

    public String getUsu_email()
    {
        return usu_email;
    }

    public void setUsu_email(String usu_email)
    {
        this.usu_email = usu_email;
    }

    public String getUsu_pass()
    {
        return usu_pass;
    }

    public void setUsu_pass(String usu_pass)
    {
        this.usu_pass = usu_pass;
    }

    public String getUsu_appaterno()
    {
        return usu_appaterno;
    }

    public void setUsu_appaterno(String usu_appaterno)
    {
        this.usu_appaterno = usu_appaterno;
    }

    public String getUsu_apmaterno()
    {
        return usu_apmaterno;
    }

    public void setUsu_apmaterno(String usu_apmaterno)
    {
        this.usu_apmaterno = usu_apmaterno;
    }

    public String getUsu_tipo()
    {
        return usu_tipo;
    }

    public void setUsu_tipo(String usu_tipo)
    {
        this.usu_tipo = usu_tipo;
    }


    public String getUsu_nombre()
    {
        return usu_nombre;
    }

    public void setUsu_nombre(String usu_nombre)
    {
        this.usu_nombre = usu_nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", usu_clave='" + usu_clave + '\'' +
                ", usu_nombre='" + usu_nombre + '\'' +
                ", usu_telefono='" + usu_telefono + '\'' +
                ", usu_email='" + usu_email + '\'' +
                ", usu_pass='" + usu_pass + '\'' +
                ", usu_appaterno='" + usu_appaterno + '\'' +
                ", usu_apmaterno='" + usu_apmaterno + '\'' +
                ", usu_tipo='" + usu_tipo + '\'' +
                '}';
    }
}
