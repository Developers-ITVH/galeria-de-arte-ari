package com.bringsolutions.arigaleriadearte.Interactor;

import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Mensaje;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.VerificarCorreo;
import com.bringsolutions.arigaleriadearte.Presenters.VerificarCorreoPresenter;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificarCorreoInteractor implements VerificarCorreo.Interactor
{

    Api api = RetrofitInstance.getApiService();
    VerificarCorreoPresenter presenter;

    public VerificarCorreoInteractor(VerificarCorreoPresenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void verificarCorreo(Usuario usuario)
    {
        Call<Mensaje> bodyCall = api.validarCorreo(usuario);
        bodyCall.enqueue(new Callback<Mensaje>()
        {
            @Override
            public void onResponse(Call<Mensaje> call, Response<Mensaje> response)
            {
               if (response.isSuccessful())
               {

                    presenter.resultCorreo(new Respuesta("Se ha enviado un código de verificación a su correo"));
               }else
                   {

                       presenter.error(new Respuesta("El correo no se encuentra registrado"));
                   }
            }

            @Override
            public void onFailure(Call<Mensaje> call, Throwable t)
            {
                presenter.error(new Respuesta("error "+t.getLocalizedMessage()));
            }
        });
    }
}
