package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.ActualizarUsuario;
import com.bringsolutions.arigaleriadearte.Presenters.EditarPerfilPresenter;
import com.bringsolutions.arigaleriadearte.Views.EditarPerfilView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarPerfilInteractor implements ActualizarUsuario.Interactor
{

    EditarPerfilPresenter presenter;
    Api api = RetrofitInstance.getApiService();


    public EditarPerfilInteractor(EditarPerfilPresenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void updateUsuario(Usuario usuario)
    {
        Call<ResponseBody> bodyCall = api.updateUser(usuario);
        bodyCall.enqueue(new Callback<ResponseBody>()
        {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                if (response.isSuccessful())
                {
                    presenter.resultUsuario(new Respuesta("Información actualizada"));
                }else
                    {
                        presenter.errorUsuario(new Respuesta("Error al actualizar usuario"));
                    }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t)
            {
                presenter.errorUsuario(new Respuesta("Error de conexion a internet " + t.getLocalizedMessage()));
            }
        });
    }
}
