package com.bringsolutions.arigaleriadearte.Interactor.Models;

import java.io.Serializable;

public class Producto implements Serializable
{
    private int id;
    private String pro_titulo;
    private String pro_descripcion;
    private String pro_foto;
    private double pro_precio;

    public Producto()
    {

    }

    public Producto(int id, String pro_titulo, String pro_descripcion, String pro_foto, double pro_precio)
    {
        this.id = id;
        this.pro_titulo = pro_titulo;
        this.pro_descripcion = pro_descripcion;
        this.pro_foto = pro_foto;
        this.pro_precio = pro_precio;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getPro_titulo()
    {
        return pro_titulo;
    }

    public void setPro_titulo(String pro_titulo)
    {
        this.pro_titulo = pro_titulo;
    }

    public String getPro_descripcion()
    {
        return pro_descripcion;
    }

    public void setPro_descripcion(String pro_descripcion)
    {
        this.pro_descripcion = pro_descripcion;
    }

    public String getPro_foto()
    {
        return pro_foto;
    }

    public void setPro_foto(String pro_foto)
    {
        this.pro_foto = pro_foto;
    }

    public double getPro_precio()
    {
        return pro_precio;
    }

    public void setPro_precio(double pro_precio)
    {
        this.pro_precio = pro_precio;
    }

    @Override
    public String toString()
    {
        return "Producto{" +
                "id=" + id +
                ", pro_titulo='" + pro_titulo + '\'' +
                ", pro_descripcion='" + pro_descripcion + '\'' +
                ", pro_foto='" + pro_foto + '\'' +
                ", pro_precio=" + pro_precio +
                '}';
    }
}
