package com.bringsolutions.arigaleriadearte.Interactor.Api;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Code;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Comentario;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Evento;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Firma;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Galeria_Virtual;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Mensaje;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Noticia;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Producto;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;

import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api
{

    //Get
    //Galeria Virtual


    //Obras
    @Headers("Content-Type: application/json")
    @GET("galeriav")
    Call<List<Galeria_Virtual>> getGaleriaVirtual();
    //Obras
    @Headers("Content-Type: application/json")
    @GET("obras")
    Call<List<Obra>> getObras();

    //Firmas
    @Headers("Content-Type: application/json")
    @GET("firmas")
    Call<List<Firma>> getFirmas();

    //Autores
    @Headers("Content-Type: application/json")
    @GET("autores")
    Call<List<Autor>> getAutores();

    //Esculturas
    @Headers("Content-Type: application/json")
    @GET("esculturas")
    Call<List<Escultura>> getEsculturas();


    //Productos
    @Headers("Content-Type: application/json")
    @GET("products")
    Call<List<Producto>> getProducts();


    //Consutar Escultura
    @Headers("Content-Type: application/json")
    @POST("esculturasclave")
    Call<List<Escultura>> getEscultura(@Body Escultura escultura);

    //Consutar Obra
    @Headers("Content-Type: application/json")
    @POST("obrasWhere")
    Call<List<Obra>> getObra(@Body Obra  obra);

    //Comentarios
    @Headers("Content-Type: application/json")
    @GET("comments")
    Call<List<Comentario>> getComentarios();

    //Usuarios
    @Headers("Content-Type: application/json")
    @GET("users")
    Call<List<Usuario>> listUsers();


    //Noticias
    @Headers("Content-Type: application/json")
    @GET("ultimasnoticias")
    Call<List<Noticia>> getNoticias ();

    //Eventos
    @Headers("Content-Type: application/json")
    @GET("ultimoseventos")
    Call<List<Evento>> getEventos ();

    //POST
    //Login
    @Headers("Content-Type: application/json")
    @POST("login")
    Call<Usuario> loginUser(@Body Usuario usuario);

    //Add Comentario
    @Headers("Content-Type: application/json")
    @POST("AddComments")
    Call<ResponseBody> addComentario(@Body Comentario comentario);

    //Registrar Usuario
    @Headers("Content-Type: application/json")
    @POST("addUsers")
    Call<Usuario> registroUser(@Body Usuario usuario);

    //Update User
    @Headers("Content-Type: application/json")
    @POST("update")
    Call<ResponseBody> updateUser(@Body Usuario usuario);



    //Reset Password

    @Headers("Content-Type: application/json")
    @POST("ressetPass")
    Call<Mensaje> validarCorreo(@Body Usuario usuario);


    @Headers("Content-Type: application/json")
    @POST("verificate")
    Call<Mensaje> validarCode(@Body Code code);


}
