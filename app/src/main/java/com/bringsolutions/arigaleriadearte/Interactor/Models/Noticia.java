package com.bringsolutions.arigaleriadearte.Interactor.Models;

public class Noticia
{
    int id;
    String not_clave;
    String not_nombre;
    String not_descripcion;
    String not_fecha;
    String not_horario;

    public Noticia(int id, String not_clave, String not_nombre, String not_descripcion, String not_fecha, String not_horario)
    {
        this.id = id;
        this.not_clave = not_clave;
        this.not_nombre = not_nombre;
        this.not_descripcion = not_descripcion;
        this.not_fecha = not_fecha;
        this.not_horario = not_horario;
    }

    public Noticia()
    {

    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getNot_clave()
    {
        return not_clave;
    }

    public void setNot_clave(String not_clave)
    {
        this.not_clave = not_clave;
    }

    public String getNot_nombre()
    {
        return not_nombre;
    }

    public void setNot_nombre(String not_nombre)
    {
        this.not_nombre = not_nombre;
    }

    public String getNot_descripcion()
    {
        return not_descripcion;
    }

    public void setNot_descripcion(String not_descripcion)
    {
        this.not_descripcion = not_descripcion;
    }

    public String getNot_fecha()
    {
        return not_fecha;
    }

    public void setNot_fecha(String not_fecha)
    {
        this.not_fecha = not_fecha;
    }

    public String getNot_horario()
    {
        return not_horario;
    }

    public void setNot_horario(String not_horario)
    {
        this.not_horario = not_horario;
    }

    @Override
    public String toString()
    {
        return "Noticia{" +
                "id=" + id +
                ", not_clave='" + not_clave + '\'' +
                ", not_nombre='" + not_nombre + '\'' +
                ", not_descripcion='" + not_descripcion + '\'' +
                ", not_fecha='" + not_fecha + '\'' +
                ", not_horario='" + not_horario + '\'' +
                '}';
    }
}
