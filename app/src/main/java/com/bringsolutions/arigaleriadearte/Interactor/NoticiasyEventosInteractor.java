package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Evento;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Noticia;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.NoticiasyEventos;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoticiasyEventosInteractor implements NoticiasyEventos.Interactor
{
    NoticiasyEventos.Presenter presenter;
    Api api = RetrofitInstance.getApiService();

    public NoticiasyEventosInteractor(NoticiasyEventos.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void cargarNoticias()
    {
        Call<List<Noticia>> noticiaCall = api.getNoticias();
        noticiaCall.enqueue(new Callback<List<Noticia>>()
        {
            @Override
            public void onResponse(Call<List<Noticia>> call, Response<List<Noticia>> response)
            {
                if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        presenter.error(new Respuesta(""));
                    }else
                        {
                            presenter.resultNoticias(response.body());
                        }
                }else
                    {
                        presenter.error(new Respuesta("No se han econtrado noticiass"));
                    }
            }

            @Override
            public void onFailure(Call<List<Noticia>> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }

    @Override
    public void cargarEventos()
    {
        Call<List<Evento>> listCall =api.getEventos();
        listCall.enqueue(new Callback<List<Evento>>()
        {
            @Override
            public void onResponse(Call<List<Evento>> call, Response<List<Evento>> response)
            {
                if (response.isSuccessful())
                {
                    presenter.resultEventos(response.body());
                }else
                    {
                        presenter.error(new Respuesta("Error al cargar los eventos !!"));
                    }
            }

            @Override
            public void onFailure(Call<List<Evento>> call, Throwable t)
            {
                presenter.error(new Respuesta("Error "+ t.getLocalizedMessage() +" !!"));
            }
        });
    }
}
