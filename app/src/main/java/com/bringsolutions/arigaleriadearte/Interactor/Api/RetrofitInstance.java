package com.bringsolutions.arigaleriadearte.Interactor.Api;



import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance
{
    private static Retrofit retrofit = null;
    private static String BASE_URL   = "http://arigaleriadearte.com/galeriaBack/public/";
    //private static String BASE_URL   = "http://192.168.1.65/";

//192.168.1.65



    public static Api getApiService()
    {
        if (retrofit == null)
        {

            /*Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();*/
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return  retrofit.create(Api.class);
    }
}
