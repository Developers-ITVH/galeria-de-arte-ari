package com.bringsolutions.arigaleriadearte.Interactor.Models;

import java.io.Serializable;

public class Obra implements Serializable
{


    int id;
    String obr_clave;
    String obr_titulo;
    String obr_autor;
    String obr_clave_autor;
    String obr_nombre;
    String obr_descripcion;
    String obr_precio;
    String obr_qr;
    String obr_anio;
    String obr_ancho;
    String obr_alto;
    String obr_tecnica;
    String obr_estado;
    String obr_clave_remodelacion;
    String obr_foto;
    String aut_nombre;
    String aut_apellidos;

    public Obra()
    {

    }

    public Obra(int id, String obr_clave, String obr_titulo, String obr_autor, String obr_clave_autor, String obr_nombre, String obr_descripcion, String obr_precio, String obr_qr, String obr_anio, String obr_ancho, String obr_alto, String obr_tecnica, String obr_estado, String obr_clave_remodelacion, String obr_foto)
    {
        this.id = id;
        this.obr_clave = obr_clave;
        this.obr_titulo = obr_titulo;
        this.obr_autor = obr_autor;
        this.obr_clave_autor = obr_clave_autor;
        this.obr_nombre = obr_nombre;
        this.obr_descripcion = obr_descripcion;
        this.obr_precio = obr_precio;
        this.obr_qr = obr_qr;
        this.obr_anio = obr_anio;
        this.obr_ancho = obr_ancho;
        this.obr_alto = obr_alto;
        this.obr_tecnica = obr_tecnica;
        this.obr_estado = obr_estado;
        this.obr_clave_remodelacion = obr_clave_remodelacion;
        this.obr_foto = obr_foto;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getObr_clave()
    {
        return obr_clave;
    }

    public void setObr_clave(String obr_clave)
    {
        this.obr_clave = obr_clave;
    }

    public String getObr_clave_autor()
    {
        return obr_clave_autor;
    }

    public void setObr_clave_autor(String obr_clave_autor)
    {
        this.obr_clave_autor = obr_clave_autor;
    }

    public String getObr_nombre()
    {
        return obr_nombre;
    }

    public void setObr_nombre(String obr_nombre)
    {
        this.obr_nombre = obr_nombre;
    }

    public String getObr_descripcion()
    {
        return obr_descripcion;
    }

    public void setObr_descripcion(String obr_descripcion)
    {
        this.obr_descripcion = obr_descripcion;
    }

    public String getObr_precio()
    {
        return obr_precio;
    }

    public void setObr_precio(String obr_precio)
    {
        this.obr_precio = obr_precio;
    }

    public String getObr_qr()
    {
        return obr_qr;
    }

    public void setObr_qr(String obr_qr)
    {
        this.obr_qr = obr_qr;
    }

    public String getObr_anio()
    {
        return obr_anio;
    }

    public void setObr_anio(String obr_anio)
    {
        this.obr_anio = obr_anio;
    }

    public String getObr_ancho()
    {
        return obr_ancho;
    }

    public void setObr_ancho(String obr_ancho)
    {
        this.obr_ancho = obr_ancho;
    }

    public String getObr_alto()
    {
        return obr_alto;
    }

    public void setObr_alto(String obr_alto)
    {
        this.obr_alto = obr_alto;
    }

    public String getObr_tecnica()
    {
        return obr_tecnica;
    }

    public void setObr_tecnica(String obr_tecnica)
    {
        this.obr_tecnica = obr_tecnica;
    }

    public String getObr_estado()
    {
        return obr_estado;
    }

    public void setObr_estado(String obr_estado)
    {
        this.obr_estado = obr_estado;
    }

    public String getObr_clave_remodelacion()
    {
        return obr_clave_remodelacion;
    }

    public void setObr_clave_remodelacion(String obr_clave_remodelacion)
    {
        this.obr_clave_remodelacion = obr_clave_remodelacion;
    }

    public String getObr_foto()
    {
        return obr_foto;
    }

    public void setObr_foto(String obr_foto)
    {
        this.obr_foto = obr_foto;
    }

    public String getObr_titulo()
    {
        return obr_titulo;
    }

    public void setObr_titulo(String obr_titulo)
    {
        this.obr_titulo = obr_titulo;
    }

    public String getObr_autor()
    {
        return obr_autor;
    }

    public void setObr_autor(String obr_autor)
    {
        this.obr_autor = obr_autor;
    }

    public String getAut_nombre()
    {
        return aut_nombre;
    }

    public void setAut_nombre(String aut_nombre)
    {
        this.aut_nombre = aut_nombre;
    }

    public String getAut_apellidos()
    {
        return aut_apellidos;
    }

    public void setAut_apellidos(String aut_apellidos)
    {
        this.aut_apellidos = aut_apellidos;
    }

    @Override
    public String toString()
    {
        return "Obra{" +
                "id=" + id +
                ", obr_clave='" + obr_clave + '\'' +
                ", obr_titulo='" + obr_titulo + '\'' +
                ", obr_autor='" + obr_autor + '\'' +
                ", obr_clave_autor='" + obr_clave_autor + '\'' +
                ", obr_nombre='" + obr_nombre + '\'' +
                ", obr_descripcion='" + obr_descripcion + '\'' +
                ", obr_precio='" + obr_precio + '\'' +
                ", obr_qr='" + obr_qr + '\'' +
                ", obr_anio='" + obr_anio + '\'' +
                ", obr_ancho='" + obr_ancho + '\'' +
                ", obr_alto='" + obr_alto + '\'' +
                ", obr_tecnica='" + obr_tecnica + '\'' +
                ", obr_estado='" + obr_estado + '\'' +
                ", obr_clave_remodelacion='" + obr_clave_remodelacion + '\'' +
                ", obr_foto='" + obr_foto + '\'' +
                ", aut_nombre='" + aut_nombre + '\'' +
                ", aut_apellidos='" + aut_apellidos + '\'' +
                '}';
    }
}

