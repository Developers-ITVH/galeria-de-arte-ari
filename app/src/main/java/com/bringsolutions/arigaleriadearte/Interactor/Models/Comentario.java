package com.bringsolutions.arigaleriadearte.Interactor.Models;

import java.io.Serializable;

public class Comentario implements Serializable
{

    /*
    *
    *  "id": 6,
        "com_clave": "32vb",
        "com_comentario": "Comentario desde postman2",
        "com_fk_usuario": 9,
        "usu_nombre": "12",
        "usu_appaterno": "edgar",
        "usu_apmaterno": "edgar",
        "usu_telefono": "2",
        "usu_email": "edgar",
        "usu_clave": "123edf"*/
    int id ;
    int com_fk_usuario;
    String com_clave;
    String usu_nombre;
    String com_comentario;

    public Comentario()
    {

    }

    public Comentario(int id, int com_fk_usuario, String com_clave, String usu_nombre, String com_comentario)
    {
        this.id = id;
        this.com_fk_usuario = com_fk_usuario;
        this.com_clave = com_clave;
        this.usu_nombre = usu_nombre;
        this.com_comentario = com_comentario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCom_clave() {
        return com_clave;
    }

    public void setCom_clave(String com_clave) {
        this.com_clave = com_clave;
    }

    public String getUsu_nombre() {
        return usu_nombre;
    }

    public void setUsu_nombre(String usu_nombre) {
        this.usu_nombre = usu_nombre;
    }

    public String getCom_comentario() {
        return com_comentario;
    }

    public void setCom_comentario(String com_comentario) {
        this.com_comentario = com_comentario;
    }

    public int getCom_fk_usuario()
    {
        return com_fk_usuario;
    }

    public void setCom_fk_usuario(int com_fk_usuario)
    {
        this.com_fk_usuario = com_fk_usuario;
    }

    @Override
    public String toString()
    {
        return "Comentario{" +
                "id=" + id +
                ", com_fk_usuario=" + com_fk_usuario +
                ", com_clave='" + com_clave + '\'' +
                ", usu_nombre='" + usu_nombre + '\'' +
                ", com_comentario='" + com_comentario + '\'' +
                '}';
    }
}
