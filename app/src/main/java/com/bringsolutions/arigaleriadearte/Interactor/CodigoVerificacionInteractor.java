package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Code;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Mensaje;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.VerificarCorreo;
import com.bringsolutions.arigaleriadearte.Presenters.CodigoVerificacionPresenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CodigoVerificacionInteractor implements VerificarCorreo.CodigoVerificacion.Interactor
{
    CodigoVerificacionPresenter presenter;
    Api api = RetrofitInstance.getApiService();

    public CodigoVerificacionInteractor(CodigoVerificacionPresenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void veriicarCode(Code code)
    {
        Call<Mensaje> codeCall = api.validarCode(code);
        codeCall.enqueue(new Callback<Mensaje>()
        {
            @Override
            public void onResponse(Call<Mensaje> call, Response<Mensaje> response)
            {
                if (response.isSuccessful())
                {
                    presenter.resultCode(new Respuesta("Obteniedo información"));

                }else
                {
                    presenter.error(new Respuesta("Codigo de verificacion erroneo"));
                }
            }

            @Override
            public void onFailure(Call<Mensaje> call, Throwable t)
            {
                presenter.error(new Respuesta("error" + t.getLocalizedMessage()));
            }
        });

    }
}
