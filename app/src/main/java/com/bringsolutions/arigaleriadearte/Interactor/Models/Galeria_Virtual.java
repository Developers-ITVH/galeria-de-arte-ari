package com.bringsolutions.arigaleriadearte.Interactor.Models;

public class Galeria_Virtual
{
    private int  gal_id;
    private String gal_titulo;
    private String gal_foto;
    private String gal_descripcion;

    public Galeria_Virtual()
    {

    }

    public Galeria_Virtual(int gal_id, String gal_titulo, String gal_foto, String gal_descripcion)
    {
        this.gal_id = gal_id;
        this.gal_titulo = gal_titulo;
        this.gal_foto = gal_foto;
        this.gal_descripcion = gal_descripcion;
    }

    public int getGal_id()
    {
        return gal_id;
    }

    public void setGal_id(int gal_id)
    {
        this.gal_id = gal_id;
    }

    public String getGal_titulo()
    {
        return gal_titulo;
    }

    public void setGal_titulo(String gal_titulo)
    {
        this.gal_titulo = gal_titulo;
    }

    public String getGal_foto()
    {
        return gal_foto;
    }

    public void setGal_foto(String gal_foto)
    {
        this.gal_foto = gal_foto;
    }

    public String getGal_descripcion()
    {
        return gal_descripcion;
    }

    public void setGal_descripcion(String gal_descripcion)
    {
        this.gal_descripcion = gal_descripcion;
    }
}
