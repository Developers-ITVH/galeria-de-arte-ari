package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Galeria_Virtual;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.GaleriaVirtual;
import com.bringsolutions.arigaleriadearte.Presenters.GaleriaVirtualPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GaleriaVirtualInteractor implements GaleriaVirtual.Interactor
{
    GaleriaVirtualPresenter presenter;
    Api api = RetrofitInstance.getApiService();

    public GaleriaVirtualInteractor(GaleriaVirtualPresenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void getGaleriaVirtual()
    {
         Call<List<Galeria_Virtual>> listCall =api.getGaleriaVirtual();
        listCall.enqueue(new Callback<List<Galeria_Virtual>>()
        {
            @Override
            public void onResponse(Call<List<Galeria_Virtual>> call, Response<List<Galeria_Virtual>> response)
            {
               if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        presenter.errorResult(new Respuesta("No se han agregado fotos a la galería virtual"));
                    }else
                        {
                            presenter.resultGaleriaVirtual(response.body());
                        }
                }else
                    {
                        presenter.errorResult(new Respuesta("Error al consultar la galería virtual"));
                    }
            }

            @Override
            public void onFailure(Call<List<Galeria_Virtual>> call, Throwable t)
            {
                presenter.errorResult(new Respuesta("Error de conexión " + t.getLocalizedMessage()));
            }
        });

    }
}
