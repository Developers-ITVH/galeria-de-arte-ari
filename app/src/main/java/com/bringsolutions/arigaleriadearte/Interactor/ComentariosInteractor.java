package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Comentario;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Comentarios;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComentariosInteractor implements Comentarios.Interactor
{
    Comentarios.Presenter presenter;
    Api api = RetrofitInstance.getApiService();

    public ComentariosInteractor(Comentarios.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void getComentarios()
    {
        Call<List<Comentario>> listCall = api.getComentarios();
        listCall.enqueue(new Callback<List<Comentario>>()
        {
            @Override
            public void onResponse(Call<List<Comentario>> call, Response<List<Comentario>> response)
            {
                if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        presenter.error(new Respuesta("No se encontraron comentarios"));
                    }else
                        {
                            presenter.resultComentarios(response.body());

                        }
                }else
                    {
                        presenter.error(new Respuesta("No se han encontrado comentarios"));
                    }
            }

            @Override
            public void onFailure(Call<List<Comentario>> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }

    @Override
    public void putComentario(final Comentario comentario)
    {

        comentario.setCom_clave("545054100");
        Call<ResponseBody> responseBodyCall = api.addComentario(comentario);
        responseBodyCall.enqueue(new Callback<ResponseBody>()
        {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                if (response.isSuccessful())
                {
                    presenter.resultComentario(new Respuesta("Comentario publicado"));
                }else
                    {
                        presenter.error(new Respuesta("Por favor intenta nuevamente"));
                        System.out.println(response.toString());
                        System.out.println(comentario.toString());
                    }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });

    }
}
