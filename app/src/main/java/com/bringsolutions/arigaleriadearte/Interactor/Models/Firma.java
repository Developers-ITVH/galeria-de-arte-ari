package com.bringsolutions.arigaleriadearte.Interactor.Models;

import java.io.Serializable;

public class Firma implements Serializable
{
    int fir_id;
    String fir_clave;
    String fir_nombre_autor;
    String fir_fecha;
    String fir_lugar_nacimiento;
    String fir_foto;

    public Firma()
    {

    }

    public Firma(String fir_nombre_autor, String fir_fecha, String fir_lugar_nacimiento, String fir_foto)
    {
        this.fir_nombre_autor = fir_nombre_autor;
        this.fir_fecha = fir_fecha;
        this.fir_lugar_nacimiento = fir_lugar_nacimiento;
        this.fir_foto = fir_foto;
    }

    public Firma(int fir_id, String fir_clave, String fir_nombre_autor, String fir_fecha, String fir_lugar_nacimiento, String fir_foto)
    {
        this.fir_id = fir_id;
        this.fir_clave = fir_clave;
        this.fir_nombre_autor = fir_nombre_autor;
        this.fir_fecha = fir_fecha;
        this.fir_lugar_nacimiento = fir_lugar_nacimiento;
        this.fir_foto = fir_foto;
    }


    public int getFir_id()
    {

        return fir_id;
    }

    public void setFir_id(int fir_id)
    {
        this.fir_id = fir_id;
    }

    public String getFir_clave()
    {
        return fir_clave;
    }

    public void setFir_clave(String fir_clave)
    {
        this.fir_clave = fir_clave;
    }

    public String getFir_nombre_autor()
    {
        return fir_nombre_autor;
    }

    public void setFir_nombre_autor(String fir_nombre_autor)
    {
        this.fir_nombre_autor = fir_nombre_autor;
    }

    public String getFir_fecha()
    {
        return fir_fecha;
    }

    public void setFir_fecha(String fir_fecha)
    {
        this.fir_fecha = fir_fecha;
    }

    public String getFir_lugar_nacimiento()
    {
        return fir_lugar_nacimiento;
    }

    public void setFir_lugar_nacimiento(String fir_lugar_nacimiento)
    {
        this.fir_lugar_nacimiento = fir_lugar_nacimiento;
    }

    public String getFir_foto()
    {
        return fir_foto;
    }

    public void setFir_foto(String fir_foto)
    {
        this.fir_foto = fir_foto;
    }

    @Override
    public String toString()
    {
        return "Firma{" +
                "fir_id=" + fir_id +
                ", fir_clave='" + fir_clave + '\'' +
                ", fir_nombre_autor='" + fir_nombre_autor + '\'' +
                ", fir_fecha='" + fir_fecha + '\'' +
                ", fir_lugar_nacimiento='" + fir_lugar_nacimiento + '\'' +
                ", fir_foto='" + fir_foto + '\'' +
                '}';
    }
}

