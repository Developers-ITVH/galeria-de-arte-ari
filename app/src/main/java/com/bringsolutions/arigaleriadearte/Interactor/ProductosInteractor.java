package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Producto;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Productos;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductosInteractor implements Productos.Interactor
{
    Productos.Presenter presenter;
    Api api = RetrofitInstance.getApiService();


    public ProductosInteractor(Productos.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void getProductos()
    {
        Call<List<Producto>> listCall = api.getProducts();
        listCall.enqueue(new Callback<List<Producto>>()
        {
            @Override
            public void onResponse(Call<List<Producto>> call, Response<List<Producto>> response)
            {
                if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        presenter.error(new Respuesta(""));
                    }else
                        {
                            presenter.cargarProductos(response.body());
                        }
                }else
                    {
                        presenter.error(new Respuesta("No se han encontrado productos por el momento"));
                    }
            }

            @Override
            public void onFailure(Call<List<Producto>> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }
}
