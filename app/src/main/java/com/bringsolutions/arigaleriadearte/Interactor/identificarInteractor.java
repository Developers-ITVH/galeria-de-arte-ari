package com.bringsolutions.arigaleriadearte.Interactor;

import android.util.Log;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Identificar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class identificarInteractor implements Identificar.Interactor
{
    Identificar.Presentador presentador;
    Api api = RetrofitInstance.getApiService();

    public identificarInteractor(Identificar.Presentador presentador)
    {
        this.presentador = presentador;
    }


    @Override
    public void consultarObra(Obra obra)
    {
      Call<List<Obra>> obraCall = api.getObra(obra);
      obraCall.enqueue(new Callback<List<Obra>>()
      {
          @Override
          public void onResponse(Call<List<Obra>> call, Response<List<Obra>> response)
          {
              if (response.isSuccessful())
              {
                  presentador.resultObra(response.body().get(0));
              }else
                  {
                      presentador.errorResult(new Respuesta("No se pudo identificar la obra"));
                  }
          }

          @Override
          public void onFailure(Call<List<Obra>> call, Throwable t)
          {
                presentador.errorResult(new Respuesta("Sin conexión" + t.getLocalizedMessage()));
          }
      });
       presentador.resultObra(obra);
    }

    @Override
    public void consultarEscultura(Escultura escultura)
    {
        Call<List<Escultura>> esculturaCall = api.getEscultura(escultura);
        esculturaCall.enqueue(new Callback<List<Escultura>>()
        {
            @Override
            public void onResponse(Call<List<Escultura>> call, Response<List<Escultura>> response)
            {
                if (response.isSuccessful())
                {
                    System.out.println("Escultura"+response.body().toString());
                    presentador.resultEscultura(response.body().get(0));
                }else
                    {
                        presentador.errorResult(new Respuesta("La escultura no se encuentra"));
                    }
            }

            @Override
            public void onFailure(Call<List<Escultura>> call, Throwable t)
            {
                presentador.errorResult(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }
}
