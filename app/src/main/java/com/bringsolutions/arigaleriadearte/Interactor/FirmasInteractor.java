package com.bringsolutions.arigaleriadearte.Interactor;

import com.bringsolutions.arigaleriadearte.Interactor.Api.Api;
import com.bringsolutions.arigaleriadearte.Interactor.Api.RetrofitInstance;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Firma;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Firmas;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirmasInteractor implements Firmas.Interactor
{
    Firmas.Presenter presenter;
    Api api = RetrofitInstance.getApiService();

    public FirmasInteractor(Firmas.Presenter presenter)
    {
        this.presenter = presenter;
    }

    @Override
    public void obtenerFirmas()
    {
        Call<List<Firma>> listCall = api.getFirmas();
        listCall.enqueue(new Callback<List<Firma>>()
        {
            @Override
            public void onResponse(Call<List<Firma>> call, Response<List<Firma>> response)
            {
                if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        presenter.error(new Respuesta("No se encontraron firmas"));
                    }else
                        {
                            presenter.resultFirmas(response.body());
                        }
                }else
                    {
                        presenter.error(new Respuesta(response.message()));
                    }
            }

            @Override
            public void onFailure(Call<List<Firma>> call, Throwable t)
            {
                presenter.error(new Respuesta(t.getLocalizedMessage()));
            }
        });
    }
}
