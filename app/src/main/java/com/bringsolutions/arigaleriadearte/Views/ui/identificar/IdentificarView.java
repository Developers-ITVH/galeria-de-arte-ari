package com.bringsolutions.arigaleriadearte.Views.ui.identificar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Identificar;
import com.bringsolutions.arigaleriadearte.Presenters.identificarPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.ZoomImageView;
import com.bumptech.glide.Glide;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.DecimalFormat;

import es.dmoral.toasty.Toasty;
import retrofit2.http.GET;


public class IdentificarView extends Fragment implements Identificar.View
{

    private Button btnEscanear;
    private RelativeLayout contenedorGneral;
    private TextView txtNombre,txtAutor,txtDescripcion,txtMaterial,txtAño,txtAnchura,txtAltura,txtTecnica,txtEstado,txtPrecio;
    ImageView imageView;
    Identificar.Presentador presentador;


    View view;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

         view = inflater.inflate(R.layout.fragment_identificar, container, false);

        //Casting
        btnEscanear = view.findViewById(R.id.btnescanear);
        contenedorGneral = view.findViewById(R.id.contendorgeneral);
        imageView = view.findViewById(R.id.imageView2);

        //EditTexts

        txtNombre = view.findViewById(R.id.txttitulo);
        txtAutor = view.findViewById(R.id.txtautor);
        txtDescripcion = view.findViewById(R.id.txtdescripcion);
        txtMaterial = view.findViewById(R.id.txtmaterial);
        txtAño = view.findViewById(R.id.txtano);
        txtAnchura = view.findViewById(R.id.txtanchura);
        txtAltura = view.findViewById(R.id.txtaltura);
        txtTecnica = view.findViewById(R.id.txttecnica);
        txtEstado = view.findViewById(R.id.txtestado);
        txtPrecio = view.findViewById(R.id.txtprecion);

        //Iniciar Presenter

        presentador = new identificarPresenter(this);
        btnEscanear.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                iniciarEscaneo();
            }
        });

       iniciarEscaneo();



        return view;
    }

    private void iniciarEscaneo()
    {
        IntentIntegrator.forSupportFragment(this).initiateScan();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);        {
        if (result != null){
            if (result.getContents() !=null)
            {
                String consultar = result.getContents().substring(0,3).trim();
                if (consultar.equals("ESC"))
                {
                    Escultura escultura = new Escultura();
                    escultura.setEsc_clave(result.getContents());
                    System.out.println("Escultura"+escultura.toString());
                    presentador.consultEscultura(escultura);

                }else if (consultar.equals("OBR"))
                {
                    Obra obra = new Obra();
                    obra.setObr_clave(result.getContents());
                    presentador.consultObra(obra);
                }

            }else
            {
                Toasty.info(getContext(), "Hubo un problema a la hora de escanear, intente nuevamente.", Toast.LENGTH_SHORT, true).show();
            }
        }else
        {
            Toasty.warning(getContext(), result.getContents(), Toast.LENGTH_SHORT, true).show();
        }
    }

    }

    @Override
    public void resultObra(Obra obra)
    {
        contenedorGneral.setVisibility(View.VISIBLE);
        txtNombre.setText(obra.getObr_titulo());
        txtAutor.setText("Autor : "+obra.getAut_nombre() + " " + obra.getAut_apellidos());
        txtDescripcion.setText(obra.getObr_descripcion());
        txtAño.setText("Año : "+obra.getObr_anio());
        txtAnchura.setText("Ancho : "+obra.getObr_ancho());
        txtAltura.setText("Alto : "+obra.getObr_alto());
        txtPrecio.setText("Precio : $"+obra.getObr_precio());
        if (txtTecnica.getVisibility()==View.GONE)
        {
            txtTecnica.setVisibility(View.VISIBLE);
        }
        txtTecnica.setText("Técnica : "+obra.getObr_tecnica() );
        Glide.with(this).load(obra.getObr_foto()).into(imageView);
        Toasty.success(getContext(), "¡Obra identificada!", Toast.LENGTH_SHORT, true).show();
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(getContext(), ZoomImageView.class).putExtra("obra",obra));
            }
        });

    }

    @Override
    public void resultEscultura(Escultura escultura)
    {
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        System.out.println("Escultura " + escultura.toString());
        contenedorGneral.setVisibility(View.VISIBLE);
        txtNombre.setText(escultura.getEsc_nombre());
        txtAutor.setText("Autor : "+escultura.getAut_nombre() + " " +escultura.getAut_apellidos());
        txtDescripcion.setText(escultura.getEsc_descripcion());
        txtMaterial.setText("Material : " +escultura.getEsc_material());
        txtAño.setText("Año "+escultura.getEsc_anio());
        txtAnchura.setText("Ancho : "+escultura.getEsc_ancho());
        txtAltura.setText("Alto : "+escultura.getEsc_alto());
        txtPrecio.setText("Precio : $ "+formateador.format(Integer.parseInt(escultura.getEsc_precio())));
        txtTecnica.setVisibility(View.GONE);
        Glide.with(this).load(escultura.getEsc_foto()).into(imageView);
        Toasty.success(getContext(), "¡Escultura identificada!", Toast.LENGTH_SHORT, true).show();
        imageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(getContext(),ZoomImageView.class).putExtra("escultura",escultura));
            }
        });

    }

    @Override
    public void errorResult(Respuesta respuesta)
    {
        Toasty.error(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }
}