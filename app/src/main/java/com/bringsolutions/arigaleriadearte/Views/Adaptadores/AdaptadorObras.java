package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.ZoomImageView;
import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.util.List;

public class AdaptadorObras  extends RecyclerView.Adapter<AdaptadorObras.ViewHolder> {
	
	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		
		private TextView tvTitulo, tvAutor, tvDescripcion, tvPrecio, tvAnio, tvAncho, tvAlto, tvTecnica, tvEstado;
		private ImageView imageView ;
		
		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			tvTitulo = itemView.findViewById(R.id.tvTitulo);
			tvAutor = itemView.findViewById(R.id.tvAutor);
			tvDescripcion = itemView.findViewById(R.id.tvDescripcion);
			tvPrecio = itemView.findViewById(R.id.tvPrecio);
			tvAnio = itemView.findViewById(R.id.tvAnio);
			tvAncho = itemView.findViewById(R.id.tvAncho);
			tvAlto = itemView.findViewById(R.id.tvAlto);
			tvTecnica = itemView.findViewById(R.id.tvTecnica);
			tvEstado = itemView.findViewById(R.id.tvEstado);
			imageView = itemView.findViewById(R.id.imageView3);
			
		}
		
	}
	
	public List<Obra> obraList;
	public Context context;
	
	public AdaptadorObras(List<Obra> obraList, Context context) {
		this.obraList = obraList;
		this.context = context;
	}
	
	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_obra_escultura,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		DecimalFormat formateador = new DecimalFormat(" $ ###,###.##");
		viewHolder.tvTitulo.setText(obraList.get(i).getObr_nombre());
		viewHolder.tvAutor.setText("Autor : "+obraList.get(i).getAut_nombre()+" "+obraList.get(i).getAut_apellidos());
		viewHolder.tvDescripcion.setText(obraList.get(i).getObr_descripcion());
		try {
			viewHolder.tvPrecio.setText("Precio : $ "+obraList.get(i).getObr_precio());

		}catch (Exception e)
		{
			viewHolder.tvPrecio.setText(e.getLocalizedMessage());
		}
		viewHolder.tvAnio.setText("Año : "+obraList.get(i).getObr_anio());
		viewHolder.tvAncho.setText("Ancho : "+obraList.get(i).getObr_ancho());
		viewHolder.tvAlto.setText("Alto : "+obraList.get(i).getObr_alto());
		viewHolder.tvTecnica.setText("Técnica: "+obraList.get(i).getObr_tecnica());

		viewHolder.tvEstado.setText(obraList.get(i).getObr_estado());

		Glide.with(context).load(obraList.get(i).getObr_foto()).placeholder(R.drawable.cargando).into(viewHolder.imageView);

		viewHolder.imageView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				context.startActivity(new Intent(context, ZoomImageView.class).putExtra("obra",obraList.get(i)));
			}
		});
	}
	
	@Override
	public int getItemCount() {
		return obraList.size();
	}
	
}
