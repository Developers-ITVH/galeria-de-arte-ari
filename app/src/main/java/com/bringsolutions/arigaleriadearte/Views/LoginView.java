package com.bringsolutions.arigaleriadearte.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.CONSTANTES;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.Login;
import com.bringsolutions.arigaleriadearte.Presenters.loginPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.google.android.material.textfield.TextInputEditText;

import es.dmoral.toasty.Toasty;

public class LoginView extends AppCompatActivity implements Login.View
{

    Login.Presenter presenter;
    Button inicarSesion;
    EditText txtTelefono,txtPassword;
    TextView tvolvide;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        //Casting
        casting();
    

    
    
    }

    private void casting()
    {
        //Componentes
        inicarSesion = findViewById(R.id.btnIniciarSesion);
        txtTelefono = findViewById(R.id.cajaTelefono);
        txtPassword = findViewById(R.id.cajaContra);
        //Presenters
        presenter = new loginPresenter(this);
        tvolvide = findViewById(R.id.txtolvidecontrasena);
        tvolvide.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
              /*  String url = "https://api.whatsapp.com/send?phone="+CONSTANTES.telefono+"&text=Olvide mi contraseña necesito de su ayuda para recuperarla";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);*/
              startActivity(new Intent(getApplicationContext(),VerificarCorreoView.class));
            }
        });
    }

    public void login(View view)
    {
        Usuario usuario = new Usuario();
        usuario.setUsu_telefono(txtTelefono.getText().toString());
        usuario.setUsu_pass(txtPassword.getText().toString());
        presenter.Login(usuario);

    }

    public void Registrar(View view)
    {
        startActivity(new Intent(LoginView.this, RegistroView.class));
    }

    @Override
    public void SetHome(Usuario usuario)
    {
        startActivity(new Intent(LoginView.this, Home.class));
        CONSTANTES.usuario = usuario;
        Toasty.success(this, "¡Bienvenido " + usuario.getUsu_nombre()+"!", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void Cargando(boolean b)
    {

        if (b)
        {
            inicarSesion.setEnabled(false);
            inicarSesion.setVisibility(View.GONE);
        //    cargando.setVisibility(View.VISIBLE);
            findViewById(R.id.lottie).setVisibility(View.VISIBLE);
        }else
            {
                inicarSesion.setEnabled(true);
                inicarSesion.setVisibility(View.VISIBLE);
            //    cargando.setVisibility(View.GONE);
                findViewById(R.id.lottie).setVisibility(View.GONE);
            }

    }

    @Override
    public void ShowError(Respuesta respuesta)
    {
        if (txtTelefono.getText().toString().isEmpty())
        {
            txtTelefono.setError("Este campo no puede estar vacío.");

        }else if (txtPassword.getText().toString().isEmpty())
        {
            txtPassword.setError("Este campo no puede estar vacío.");
        }
        Toasty.error(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }
}
