package com.bringsolutions.arigaleriadearte.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Firma;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Producto;
import com.bringsolutions.arigaleriadearte.R;
import com.bumptech.glide.Glide;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ZoomImageView extends AppCompatActivity
{

    ImageView imageView;
    PhotoViewAttacher photoViewAttacher;
    Obra obra;
    Escultura  escultura;
    Autor autor;
    Producto producto;
    Firma firma;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image_view);
        imageView = findViewById(R.id.imageView6);
      //  getSupportActionBar().hide();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        obra =(Obra) getIntent().getExtras().getSerializable("obra");
        escultura =(Escultura) getIntent().getExtras().getSerializable("escultura");
        autor =(Autor) getIntent().getExtras().getSerializable("autor");
        producto =(Producto) getIntent().getExtras().getSerializable("producto");
        firma = (Firma) getIntent().getExtras().getSerializable("firma");

        if (obra!=null)
        {
            Glide.with(getApplicationContext()).load(obra.getObr_foto()).into(imageView);

        }

        if (escultura!=null)
        {
            Glide.with(getApplicationContext()).load(escultura.getEsc_foto()).into(imageView);

        }

        if (autor!=null)
        {
            Glide.with(getApplicationContext()).load(autor.getAut_foto()).into(imageView);

        }

        if (producto!=null)
        {
            Glide.with(getApplicationContext()).load(producto.getPro_foto()).into(imageView);
        }

        if (firma!=null)
        {
            Glide.with(getApplicationContext()).load(firma.getFir_foto()).into(imageView);
        }


        photoViewAttacher = new PhotoViewAttacher(imageView);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
