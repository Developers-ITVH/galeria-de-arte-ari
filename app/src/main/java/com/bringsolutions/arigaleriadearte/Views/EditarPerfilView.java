package com.bringsolutions.arigaleriadearte.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.CONSTANTES;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.ActualizarUsuario;
import com.bringsolutions.arigaleriadearte.Presenters.EditarPerfilPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.google.android.material.textfield.TextInputEditText;

import es.dmoral.toasty.Toasty;

public class EditarPerfilView extends AppCompatActivity  implements ActualizarUsuario.View
{

    TextInputEditText txtNombre,txtApellidoP,txtApellidoM,txtTelefono,txtEmail,txtPassword,txtPassword2;
    Button btnGuardar;
    Usuario usuario;
    EditarPerfilPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        usuario = CONSTANTES.usuario;

        casting();
        cargarDatosUsuario();
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtPassword.getText().toString().isEmpty()||txtPassword2.getText().toString().isEmpty())
                {
                    Toasty.error(getApplicationContext(),"Intoduzca una contraseña", Toast.LENGTH_SHORT, true).show();
                }else if (txtPassword.getText().toString().equals(txtPassword2.getText().toString()))
                {
                    //Toast.makeText(EditarPerfilView.this, "Todo correcto", Toast.LENGTH_SHORT).show();
                    if (txtNombre.getText().toString().isEmpty()||txtApellidoP.getText().toString().isEmpty()||txtApellidoM.getText().toString().isEmpty()||txtTelefono.getText().toString().isEmpty()||txtEmail.getText().toString().isEmpty())
                    {
                        Toasty.error(getApplicationContext(),"Algún campo vacío", Toast.LENGTH_SHORT, true).show();
                    }else
                        {
                            usuario.setUsu_nombre(txtNombre.getText().toString());
                            usuario.setUsu_appaterno(txtApellidoP.getText().toString());
                            usuario.setUsu_apmaterno(txtApellidoM.getText().toString());
                            usuario.setUsu_telefono(txtTelefono.getText().toString());
                            usuario.setUsu_email(txtEmail.getText().toString());
                            usuario.setUsu_pass(txtPassword2.getText().toString());
                            System.out.println(usuario.toString());
                            presenter.updateUsuario(usuario);
                        }

                }else {
                    Toasty.error(getApplicationContext(),"Las contraseñas no coinciden", Toast.LENGTH_SHORT, true).show();
                }
            }

        });
    }

    private void cargarDatosUsuario()
    {
        txtNombre.setText(usuario.getUsu_nombre());
        txtApellidoP.setText(usuario.getUsu_appaterno());
        txtApellidoM.setText(usuario.getUsu_apmaterno());
        txtTelefono.setText(usuario.getUsu_telefono());
        txtEmail.setText(usuario.getUsu_email());
        txtPassword.setText(usuario.getUsu_pass());
        txtPassword2.setText(usuario.getUsu_pass());
    }

    private void casting()
    {
        presenter = new EditarPerfilPresenter(this);
        txtNombre = findViewById(R.id.txtnombre);
        txtApellidoP = findViewById(R.id.txtapellido_p);
        txtApellidoM = findViewById(R.id.txtapellido_m);
        txtTelefono = findViewById(R.id.txttelefono);
        txtEmail = findViewById(R.id.txtemail);
        txtPassword = findViewById(R.id.txtpassword);
        txtPassword2 = findViewById(R.id.txtpassword2);
        btnGuardar = findViewById(R.id.btnregistro);

    }


    @Override
    public void resultUpdate(Respuesta respuesta)
    {
        Toast.makeText(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void Cargando(boolean b)
    {

    }

    @Override
    public void ShowError(Respuesta respuesta)
    {
        Toast.makeText(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
