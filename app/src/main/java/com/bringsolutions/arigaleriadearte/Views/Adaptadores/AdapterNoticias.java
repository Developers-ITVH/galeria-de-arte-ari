package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Noticia;
import com.bringsolutions.arigaleriadearte.R;

import java.util.List;

public class AdapterNoticias extends RecyclerView.Adapter<AdapterNoticias.ViewHolder> {
	
	
	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		
		private TextView tvTitulo, getTvDescripcion, tvFecha, tvHorario;
		
		public ViewHolder(View itemView) {
			super(itemView);
			tvTitulo = itemView.findViewById(R.id.tvTituloNoticia);
			getTvDescripcion = itemView.findViewById(R.id.tvDescripcionNoticia);
			tvFecha = itemView.findViewById(R.id.tvFechaNoticia);
			tvHorario = itemView.findViewById(R.id.tvHorarioNoticia);
			
		}
		
	}
	
	public List<Noticia> noticiaList;
	public Context context;
	
	public AdapterNoticias(List<Noticia> noticiaList, Context context) {
		this.noticiaList = noticiaList;
		this.context = context;
	}
	
	@NonNull
	@Override
	public AdapterNoticias.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_noticia,viewGroup,false);
		
		return new AdapterNoticias.ViewHolder(view);
	}
	
	@Override
	public void onBindViewHolder(@NonNull ViewHolder viewholder, int position) {
			viewholder.tvTitulo.setText(noticiaList.get(position).getNot_nombre());
			viewholder.getTvDescripcion.setText(noticiaList.get(position).getNot_descripcion());
			viewholder.tvFecha.setText(noticiaList.get(position).getNot_fecha());
			viewholder.tvHorario.setText(noticiaList.get(position).getNot_horario());
	}
	
	
	
	@Override
	public int getItemCount() {
		return noticiaList.size();
	}
	
	
	
}
