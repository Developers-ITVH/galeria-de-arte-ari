package com.bringsolutions.arigaleriadearte.Views.ui.autores;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Autores;
import com.bringsolutions.arigaleriadearte.Presenters.AutoresPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorAutores;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorFirmas;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class AutoresView extends Fragment implements Autores.View
{

    RecyclerView recyclerAutores;
    Autores.Presenter presenter;
    View view;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

         view = inflater.inflate(R.layout.fragment_autores, container, false);
        recyclerAutores = view.findViewById(R.id.recyclerautores);

         //Presenetr

        presenter = new AutoresPresenter(this);
        presenter.cargarAutores();
        return view;
    }

    @Override
    public void cargarAutores(List<Autor> autors)
    {
        recyclerAutores.setLayoutManager(new GridLayoutManager(getContext(),2));
        AdaptadorAutores adapter;
        adapter = new AdaptadorAutores(autors,getContext());
        recyclerAutores.setAdapter(adapter);
    }

    @Override
    public void error(Respuesta respuesta)
    {
        Toasty.error(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void cargando(Boolean aBoolean)
    {
        if (aBoolean)
        {
            recyclerAutores.setVisibility(View.GONE);
            view.findViewById(R.id.lottieautores).setVisibility(View.VISIBLE);
        }else
            {
                recyclerAutores.setVisibility(View.VISIBLE);
                view.findViewById(R.id.lottieautores).setVisibility(View.GONE);
            }
    }
}