package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Evento;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Noticia;
import com.bringsolutions.arigaleriadearte.R;

import java.util.List;

public class AdapterEventos extends RecyclerView.Adapter<AdapterEventos.ViewHolder> {


	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{

		private TextView tvTitulo, getTvDescripcion, tvFecha, tvHorario;

		public ViewHolder(View itemView) {
			super(itemView);
			tvTitulo = itemView.findViewById(R.id.tvTituloEvento);
			getTvDescripcion = itemView.findViewById(R.id.tvDescripcionEvento);
			tvFecha = itemView.findViewById(R.id.tvFechaEvento);
			tvHorario = itemView.findViewById(R.id.tvHorarioEvento);
		}

	}

	public List<Evento> eventoList;
	public Context context;


	public AdapterEventos(List<Evento> eventoList, Context context)
	{
		this.eventoList = eventoList;
		this.context = context;
	}

	@NonNull
	@Override
	public AdapterEventos.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_evento,viewGroup,false);

		return new AdapterEventos.ViewHolder(view);
	}
	
	@Override
	public void onBindViewHolder(@NonNull ViewHolder viewholder, int position) {
			viewholder.tvTitulo.setText(eventoList.get(position).getEve_nombre());
			viewholder.getTvDescripcion.setText(eventoList.get(position).getEve_descripcion());
			viewholder.tvFecha.setText(eventoList.get(position).getEve_fecha());
			viewholder.tvHorario.setText(eventoList.get(position).getEve_horario());
	}
	
	
	
	@Override
	public int getItemCount() {
		return eventoList.size();
	}
	
	
	
}
