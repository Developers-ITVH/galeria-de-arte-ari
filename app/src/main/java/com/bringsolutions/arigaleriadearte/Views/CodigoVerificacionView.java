package com.bringsolutions.arigaleriadearte.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Code;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Login;
import com.bringsolutions.arigaleriadearte.Interfaces.VerificarCorreo;
import com.bringsolutions.arigaleriadearte.Presenters.CodigoVerificacionPresenter;
import com.bringsolutions.arigaleriadearte.R;

import es.dmoral.toasty.Toasty;

public class CodigoVerificacionView extends AppCompatActivity implements VerificarCorreo.CodigoVerificacion.View
{

    CodigoVerificacionPresenter presenter;
    Button button;
    EditText txtcode;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codigo_verificacion_view);
        getSupportActionBar().hide();
        casting();
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Code code = new Code();
                code.setCode(txtcode.getText().toString());
                presenter.verificarCode(code);
            }
        });
    }

    private void casting()
    {
        presenter = new CodigoVerificacionPresenter(this);
        button = findViewById(R.id.btncodigo);
        txtcode = findViewById(R.id.txtcode);
    }

    @Override
    public void resultCode(Respuesta respuesta)
    {
        Toasty.success(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, LoginView.class));
    }

    @Override
    public void error(Respuesta respuesta)
    {
        Toasty.error(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void cargando(Boolean aBoolean)
    {
        if (aBoolean)
        {
            findViewById(R.id.lottieverificarcode).setVisibility(View.VISIBLE);
        }else
            {
                findViewById(R.id.lottieverificarcode).setVisibility(View.GONE);
            }
    }
}
