package com.bringsolutions.arigaleriadearte.Views.ui.comentarios;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.CONSTANTES;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Comentario;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Comentarios;
import com.bringsolutions.arigaleriadearte.Presenters.ComentariosPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorComentarios;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorProductos;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class ComentariosView extends Fragment implements Comentarios.View
{
  View view;
  RecyclerView recyclerView;
  Comentarios.Presenter presenter;
  EditText caja;
  FloatingActionButton enviar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
       view = inflater.inflate(R.layout.fragment_comentarios_view, container, false);
        recyclerView = view.findViewById(R.id.recyclercomentarios);
        caja = view.findViewById(R.id.txtcaja);
        enviar = view.findViewById(R.id.btnenviar);
        presenter = new ComentariosPresenter(this);
        presenter.obtenerComentarios();


        enviar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (caja.getText().toString().isEmpty())
                {
                    Toasty.info(getContext(), "Escriba un mensaje", Toast.LENGTH_SHORT, true).show();
                }else
                    {
                        Comentario comentario = new Comentario();
                        comentario.setCom_comentario(caja.getText().toString());
                        comentario.setCom_fk_usuario(CONSTANTES.usuario.getId());
                        presenter.sendComentario(comentario);
                    }
            }
        });


        return view;
    }

    @Override
    public void error(Respuesta respuesta)
    {
        Toasty.error(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void cargarComentarios(List<Comentario> comentarios)
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdaptadorComentarios  adaptadorComentarios;
        adaptadorComentarios = new AdaptadorComentarios(comentarios, getContext());
        recyclerView.setAdapter(adaptadorComentarios);
        recyclerView.smoothScrollToPosition(adaptadorComentarios.getItemCount()-1);
    }

    @Override
    public void resultcomentario(Respuesta respuesta)
    {
        Toasty.warning(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
        presenter.obtenerComentarios();
        caja.setText(null);
    }

    @Override
    public void cargando(boolean b)
    {
        if (b)
        {
            recyclerView.setVisibility(View.GONE);
            view.findViewById(R.id.lottiecomentarios).setVisibility(View.VISIBLE);
        }else
            {
                recyclerView.setVisibility(View.VISIBLE);
                view.findViewById(R.id.lottiecomentarios).setVisibility(View.GONE);
            }
    }
}
