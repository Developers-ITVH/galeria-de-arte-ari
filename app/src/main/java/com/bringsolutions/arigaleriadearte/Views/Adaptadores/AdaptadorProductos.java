package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.CONSTANTES;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Producto;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.ZoomImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class AdaptadorProductos extends RecyclerView.Adapter<AdaptadorProductos.ViewHolder> {

	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{

		private TextView tvTitulo,tvPrecio;
		private ImageView foto;
		private Button btnMasinfo;

		public ViewHolder(View itemView) {
			super(itemView);
			// elementos
			tvTitulo = itemView.findViewById(R.id.tvtitulo);
			tvPrecio = itemView.findViewById(R.id.tvprecio);
			foto = itemView.findViewById(R.id.foto);
			btnMasinfo = itemView.findViewById(R.id.button);


		}

	}

	public List<Producto> productoList;
	public Context context;


	public AdaptadorProductos(List<Producto> productoList, Context context)
	{
		this.productoList = productoList;
		this.context = context;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_producto,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {


		viewHolder.tvTitulo.setText(productoList.get(i).getPro_titulo());
		viewHolder.tvPrecio.setText("$ "+productoList.get(i).getPro_precio());
		Glide.with(context).load(productoList.get(i).getPro_foto()).apply(RequestOptions.centerInsideTransform()).placeholder(R.drawable.cargando).into(viewHolder.foto);
		viewHolder.btnMasinfo.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String url = "https://api.whatsapp.com/send?phone="+ CONSTANTES.telefono +"&text=Estoy interesado en el producto "+productoList.get(i).getPro_titulo();
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				context.startActivity(i);
			}
		});

		viewHolder.foto.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				context.startActivity(new Intent(context, ZoomImageView.class).putExtra("producto",productoList.get(i)));
			}
		});

	}
	
	@Override
	public int getItemCount() {
		return productoList.size();
	}
	
}
