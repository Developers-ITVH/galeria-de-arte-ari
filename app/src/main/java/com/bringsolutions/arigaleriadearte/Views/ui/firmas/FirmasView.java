package com.bringsolutions.arigaleriadearte.Views.ui.firmas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Firma;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Firmas;
import com.bringsolutions.arigaleriadearte.Presenters.FirmasPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorFirmas;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorGaleriaVirtual;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class FirmasView extends Fragment implements Firmas.View
{
    View view;
    RecyclerView recyclerfirmas;
    Firmas.Presenter presenter;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //Presenter

        presenter = new FirmasPresenter(this);
        view= inflater.inflate(R.layout.fragment_firmas, container, false);
        recyclerfirmas = view.findViewById(R.id.recyclerfirmas);

        presenter.obtenerFirmas();

        return view;
    }

    @Override
    public void resultFirmas(List<Firma> firmaList)
    {


        recyclerfirmas.setLayoutManager(new LinearLayoutManager(getContext()));
        AdaptadorFirmas adapter;
        adapter = new AdaptadorFirmas(firmaList,getContext());
        recyclerfirmas.setAdapter(adapter);
    }

    @Override
    public void error(Respuesta respuesta)
    {
        Toasty.error(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void cargando(Boolean aBoolean)
    {
        if (aBoolean)
        {
            recyclerfirmas.setVisibility(View.GONE);
            view.findViewById(R.id.lottiefirmas).setVisibility(View.VISIBLE);
        }else
        {
            recyclerfirmas.setVisibility(View.VISIBLE);
            view.findViewById(R.id.lottiefirmas).setVisibility(View.GONE);
        }
    }
}