package com.bringsolutions.arigaleriadearte.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.VerificarCorreo;
import com.bringsolutions.arigaleriadearte.Presenters.VerificarCorreoPresenter;
import com.bringsolutions.arigaleriadearte.R;

import es.dmoral.toasty.Toasty;

public class VerificarCorreoView extends AppCompatActivity implements VerificarCorreo.View
{

    VerificarCorreoPresenter presenter;
    Button btnVerificar;
    EditText  txtCorreo;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificar_correo_view);
        getSupportActionBar().hide();


        casting();
        btnVerificar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Usuario usuario = new Usuario();
                usuario.setUsu_email(txtCorreo.getText().toString());
                presenter.verificarCorreo(usuario);
            }
        });
    }

    private void casting()
    {
        presenter = new VerificarCorreoPresenter(this);
        btnVerificar = findViewById(R.id.btnenviar);
        txtCorreo = findViewById(R.id.txtcorreorecover);
    }

    @Override
    public void resultCorreo(Respuesta respuesta)
    {
        Toast.makeText(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(),CodigoVerificacionView.class));
    }

    @Override
    public void error(Respuesta respuesta)
    {
        Toast.makeText(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void cargando(Boolean aBoolean)
    {
        if (aBoolean)
        {
            findViewById(R.id.lottieverificarcorreo).setVisibility(View.VISIBLE);
        }else
            {
                findViewById(R.id.lottieverificarcorreo).setVisibility(View.GONE);
            }
    }
}
