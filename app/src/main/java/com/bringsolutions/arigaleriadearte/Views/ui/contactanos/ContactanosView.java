package com.bringsolutions.arigaleriadearte.Views.ui.contactanos;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.CONSTANTES;
import com.bringsolutions.arigaleriadearte.R;

import es.dmoral.toasty.Toasty;


public class ContactanosView extends Fragment
{

    View view;
    ImageView btnTelefono,btnCorreo ,btnFacebook,btnWhatsaap;
    TextView tvTelefono1,tvTelefono2,tvCorreo,tvFacebook,tvWhatsaap;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view =  inflater.inflate(R.layout.fragment_contactanos_view, container, false);
        //Botones
        btnTelefono = view.findViewById(R.id.telefono);
        btnCorreo = view.findViewById(R.id.correo);
        btnFacebook = view.findViewById(R.id.facebook);
        btnWhatsaap = view.findViewById(R.id.btnwhatsaap);
        //Texview
        tvTelefono1 = view.findViewById(R.id.textView5);
        tvTelefono2 = view.findViewById(R.id.textView8);
        tvCorreo = view.findViewById(R.id.textView6);
        tvFacebook = view.findViewById(R.id.textView7);
        tvWhatsaap = view.findViewById(R.id.tvwhatsaap);

        btnTelefono.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                    Telefono();
            }
        });

        tvTelefono1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Telefono();
            }
        });

        tvTelefono2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Telefono();
            }
        });
        btnCorreo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Email();
            }
        });
        tvCorreo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Email();
            }
        });
        btnFacebook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Facebook();
            }
        });
        tvFacebook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Facebook();
            }
        });

        btnWhatsaap.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Whatsaap();
            }
        });

        tvWhatsaap.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Whatsaap();
            }
        });
        return view;
    }

    private void Whatsaap()
    {
        String url = "https://api.whatsapp.com/send?phone="+CONSTANTES.telefono+"&text=Hola Galeria de arte !!!";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void Telefono ()
    {
       try
       {
           Intent i = new Intent(Intent.ACTION_CALL);
           i.setData(Uri.parse("tel:"+ CONSTANTES.telefono));
           startActivity(i);

       }catch (Exception e)
       {

       }

    }

    public void Email ()
    {
       /* Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","contacto@arigaleria.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Android APP - ");
       startActivity(Intent.createChooser(emailIntent,  "Mensaje"));   */
        String[] TO = {"contacto@arigaleria.com"}; //Direcciones email  a enviar.
        String[] CC = {""}; //Direcciones email con copia.
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Ari Galeria de Arte");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hola !! Galeria de Arte"); // * configurar email aquí!

        try {
            startActivity(Intent.createChooser(emailIntent, "Enviar email."));
            Log.i("EMAIL", "Enviando email...");
        }
        catch (android.content.ActivityNotFoundException e) {
            Toast.makeText(getContext(), "NO existe ningún cliente de email instalado!.", Toast.LENGTH_SHORT).show();
        }
    }



    public void Facebook ()
    {


        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(getContext());
        facebookIntent.setData(Uri.parse(facebookUrl));
        startActivity(facebookIntent);

    }

    public String getFacebookPageURL(Context Context)
    {


          String FACEBOOK_URL = "https://facebook.com/ARIGaleriadeArte/";
          String FACEBOOK_PAGE_ID = "ARIGaleriadeArte";

        PackageManager packageManager = Context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

}
