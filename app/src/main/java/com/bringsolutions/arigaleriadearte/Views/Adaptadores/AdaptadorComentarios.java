package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Comentario;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.ZoomImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class AdaptadorComentarios extends RecyclerView.Adapter<AdaptadorComentarios.ViewHolder> {

	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{

		private TextView tvNombre,tvDescripcion;
		private ImageView imageView;

		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			tvNombre = itemView.findViewById(R.id.tvnombre);
			tvDescripcion = itemView.findViewById(R.id.tvDescripcion);
			imageView = itemView.findViewById(R.id.imageavatar);


		}

	}

	public List<Comentario> comentarioList;
	public Context context;


	public AdaptadorComentarios(List<Comentario> comentarioList, Context context) {
		this.comentarioList = comentarioList;
		this.context = context;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_comentario,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		viewHolder.tvNombre.setText(comentarioList.get(i).getUsu_nombre());
		viewHolder.tvDescripcion.setText(comentarioList.get(i).getCom_comentario());
	}
	
	@Override
	public int getItemCount() {
		return comentarioList.size();
	}
	
}
