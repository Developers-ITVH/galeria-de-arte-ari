package com.bringsolutions.arigaleriadearte.Views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.Registro;
import com.bringsolutions.arigaleriadearte.Presenters.registroPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.google.android.material.textfield.TextInputEditText;

import es.dmoral.toasty.Toasty;

public class RegistroView extends AppCompatActivity implements Registro.View
{
    TextInputEditText txtNombre,txtApellidoPaterno,txtApellidoMaterno,txtTelefono,txtEmail,txtPaswoord,getTxtPaswoord2;
    Button registrar;
    ProgressBar progressBar;
    Registro.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_view);
        getSupportActionBar().hide();

        casting();
    }

    public void Registrar(View view)
    {

        /*
        *
        * "usu_clave":"loquesea",
            "usu_telefono": "123456789",
            "usu_email": "root@root.com",
            "usu_nombre": "edgar",
            "usu_appaterno": "edgar",
            "usu_apmaterno": "edgar",
            "usu_pass":"root",
            "usu_tipo": "1"
        * */
        Usuario usuario = new Usuario();
        usuario.setUsu_clave("USU"+(int)Math.random()*1000);
        usuario.setUsu_nombre(txtNombre.getText().toString());
        usuario.setUsu_appaterno(txtApellidoPaterno.getText().toString());
        usuario.setUsu_telefono(txtTelefono.getText().toString());
        usuario.setUsu_apmaterno(txtApellidoMaterno.getText().toString());
        usuario.setUsu_email(txtEmail.getText().toString());
        usuario.setUsu_tipo("1");

        if (txtPaswoord.getText().toString().equals(getTxtPaswoord2.getText().toString()))
        {
            usuario.setUsu_pass(txtPaswoord.getText().toString());
            if (txtNombre.getText().toString().isEmpty()||txtApellidoPaterno.getText().toString().isEmpty()||txtApellidoMaterno.getText().toString().isEmpty()||txtEmail.getText().toString().isEmpty()||txtTelefono.getText().toString().isEmpty())
            {
                ShowError(new Respuesta("Algún campo está vacío."));
            }else
                {
                    presenter.Registro(usuario);

                }
        }else
            {
                txtPaswoord.setError("Las contraseñas no coinciden");
                getTxtPaswoord2.setError("Las contraseñas no coinciden")  ;
                ShowError(new Respuesta("Las contraseñas no coinciden "));
            }
    }

    private void casting()
    {
        //Componentes
        txtNombre = findViewById(R.id.txtnombre);
        txtApellidoPaterno = findViewById(R.id.txtapellido_p);
        txtApellidoMaterno = findViewById(R.id.txtapellido_m);
        txtTelefono = findViewById(R.id.txttelefono);
        txtEmail = findViewById(R.id.txtemail);
        txtPaswoord = findViewById(R.id.txtpassword);
        getTxtPaswoord2 = findViewById(R.id.txtpassword2);
        registrar = findViewById(R.id.btnregistro);
        progressBar = findViewById(R.id.progressBar2);

        //´Presenter

        presenter = new registroPresenter(this);


    }

    @Override
    public void SetHome(Usuario usuario)
    {
        startActivity(new Intent(RegistroView.this, Home.class));
        Toasty.success(this, "¡Bienvenido " + usuario.getUsu_nombre()+"!", Toast.LENGTH_SHORT, true).show();

        // Toast.makeText(this, "¡Bienvenido " + usuario.getUsu_nombre()+"!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void Cargando(boolean b)
    {
        if (b)
        {
            registrar.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }else
            {
                progressBar.setVisibility(View.GONE);
                registrar.setVisibility(View.VISIBLE);
            }
    }

    @Override
    public void ShowError(Respuesta respuesta)
    {
        if (txtNombre.getText().toString().isEmpty())
        {
            txtNombre.setError("Introduzca un nombre");
        }
        if (txtApellidoPaterno.getText().toString().isEmpty())
        {
            txtApellidoPaterno.setError("Introduzca un apellido");
        }
        if (txtApellidoMaterno.getText().toString().isEmpty())
        {
            txtApellidoMaterno.setError("Introduzca un apellido");
        }
        if (txtEmail.getText().toString().isEmpty())
        {
            txtEmail.setError("Introduzca un correo electrónico");
        }
        if (txtTelefono.getText().toString().isEmpty())
        {
            txtTelefono.setError("Introduzca un teléfono");
        }
        if (txtPaswoord.getText().toString().isEmpty())
        {
            txtPaswoord.setError("Introduzca una contraseña");
        }
        if (getTxtPaswoord2.getText().toString().isEmpty())
        {
            getTxtPaswoord2.setError("Introduzca una contraseña");
        }
        //Toast.makeText(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();

        Toasty.error(this, respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();

    }
}
