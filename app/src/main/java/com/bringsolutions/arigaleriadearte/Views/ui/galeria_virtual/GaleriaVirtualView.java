package com.bringsolutions.arigaleriadearte.Views.ui.galeria_virtual;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Galeria_Virtual;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.GaleriaVirtual;
import com.bringsolutions.arigaleriadearte.Presenters.GaleriaVirtualPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorGaleriaVirtual;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdapterNoticias;
import com.gjiazhe.panoramaimageview.GyroscopeObserver;
import com.gjiazhe.panoramaimageview.PanoramaImageView;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class GaleriaVirtualView extends Fragment implements GaleriaVirtual.View
{

    RecyclerView recyclerViewGaleriaVirtual;
    View view;
    GaleriaVirtualPresenter presenter;

    private AdaptadorGaleriaVirtual adapterGaleriaVirtual;
    List<Galeria_Virtual> LISTA_FOTOS_GALERIA_VIRTUAL = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_galleria_virtual, container, false);
        presenter = new GaleriaVirtualPresenter(this);
        recyclerViewGaleriaVirtual = view.findViewById(R.id.recycler_fotos_panoramicas);
        recyclerViewGaleriaVirtual.setLayoutManager(new LinearLayoutManager(getActivity()));

        //poblarRecycler();
        presenter.getGaleriaVirtual();

        return view;
    }




    @Override
    public void resultGaleriaVirtual(List<Galeria_Virtual> galeria_virtuals)
    {

        adapterGaleriaVirtual = new AdaptadorGaleriaVirtual(galeria_virtuals,getActivity());
        recyclerViewGaleriaVirtual.setAdapter(adapterGaleriaVirtual);
    }

    @Override
    public void shoError(Respuesta respuesta)
    {
        Toasty.success(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void cargando(boolean b)
    {
        if (b)
        {
            recyclerViewGaleriaVirtual.setVisibility(View.GONE);
            view.findViewById(R.id.lottiegaleriavirtual).setVisibility(View.VISIBLE);
        }else
            {
                recyclerViewGaleriaVirtual.setVisibility(View.VISIBLE);
                view.findViewById(R.id.lottiegaleriavirtual).setVisibility(View.GONE);
            }
    }
}