package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Galeria_Virtual;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.VistaFotoPanoramica;
import com.bumptech.glide.Glide;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class AdaptadorGaleriaVirtual extends RecyclerView.Adapter<AdaptadorGaleriaVirtual.ViewHolder> {

	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{

		private TextView tvTitulo,tvDescripcion;
		private ImageView imageView;

		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			tvTitulo = itemView.findViewById(R.id.txttitulo);
			imageView = itemView.findViewById(R.id.imgfotogaleriavirtual);
			tvDescripcion = itemView.findViewById(R.id.tvDescripcionFotoGaleriaVirtual);


		}

	}

	public List<Galeria_Virtual> galeria_virtuals;
	public Context context;

	public AdaptadorGaleriaVirtual(List<Galeria_Virtual> galeria_virtuals, Context context)
	{
		this.galeria_virtuals = galeria_virtuals;
		this.context = context;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

			View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_galeria_virtual,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {

		viewHolder.tvTitulo.setText(galeria_virtuals.get(i).getGal_titulo());
		viewHolder.tvDescripcion.setText(galeria_virtuals.get(i).getGal_descripcion());

		try
		{
			Glide.with(context).load(galeria_virtuals.get(i).getGal_foto()).into(viewHolder.imageView);

		}catch (Exception e )
		{
			System.out.println("error " +e.getLocalizedMessage());
		}


			viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					Intent intent = new Intent(context, VistaFotoPanoramica.class);
					intent.putExtra("url_foto", galeria_virtuals.get(i).getGal_foto());

					context.startActivity(intent);
				}
			});

	}
	
	@Override
	public int getItemCount() {
		return galeria_virtuals.size();
	}


	public static Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			System.out.println("EAST: " +e.toString());
			return null;
		}
	}


	public static Bitmap getImageBitmapFromUrl(URL url)
	{
		Bitmap bm = null;
		try {
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			if(conn.getResponseCode() != 200)
			{
				return bm;
			}
			conn.connect();
			InputStream is = conn.getInputStream();

			BufferedInputStream bis = new BufferedInputStream(is);
			try
			{
				bm = BitmapFactory.decodeStream(bis);
			}
			catch(OutOfMemoryError ex)
			{
				bm = null;
			}
			bis.close();
			is.close();
		} catch (Exception e) {}

		return bm;
	}
}
