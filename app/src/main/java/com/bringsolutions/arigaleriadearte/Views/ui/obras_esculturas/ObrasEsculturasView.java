package com.bringsolutions.arigaleriadearte.Views.ui.obras_esculturas;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Obras_Esculturas;
import com.bringsolutions.arigaleriadearte.Presenters.ObrasEsculturasPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorEsculturas;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorObras;

import java.util.List;

import es.dmoral.toasty.Toasty;


public class ObrasEsculturasView extends Fragment implements Obras_Esculturas.View
{
    View view;
    RecyclerView rvObjetos;
    
    Spinner spTipoObejtos;
    String TAG = "Obras y Esculturas";

    Obras_Esculturas.Presenter presenter;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_obras_y_esculturas, container, false);
        inicializarElmentos();
     
        clicks();

        presenter.obtenerObras();

        return view;
    }
    
    private void clicks() {
        spTipoObejtos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            
                switch (position){
                    case 0://cuadros
                       presenter.obtenerObras();
                        
                        break;
                    case 1://esculturas
                        presenter.obtenerEsculturas();
    
                        break;
                   
                }
            
            }
        
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            
            }
        });
    
        
    }
    

    private void inicializarElmentos()
    {
        rvObjetos = view.findViewById(R.id.recyclerObrasEsculturas);
        spTipoObejtos = view.findViewById(R.id.spTipoObjetos);

        //Presenter

        presenter = new ObrasEsculturasPresenter(this);
    }

    @Override
    public void cargandoDatos(Boolean aBoolean)
    {
        if (aBoolean)
        {
            rvObjetos.setVisibility(View.GONE);
            view.findViewById(R.id.lottieobrasyesculturas).setVisibility(View.VISIBLE);
        }else
            {
                rvObjetos.setVisibility(View.VISIBLE);
                view.findViewById(R.id.lottieobrasyesculturas).setVisibility(View.GONE);
            }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        Toasty.error(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }
    
    @Override
    public void cargarObras(List<Obra> OBRAS) {


        rvObjetos.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdaptadorObras adapter;
        adapter = new AdaptadorObras(OBRAS, getContext());
        rvObjetos.setAdapter(adapter);

       
    }
    
    @Override
    public void cargarEscultura(List<Escultura> esculturas)
    {

        rvObjetos.setLayoutManager(new LinearLayoutManager(getActivity()));
        AdaptadorEsculturas adapter;
        adapter = new AdaptadorEsculturas(esculturas, getContext());
        rvObjetos.setAdapter(adapter);

    }
    
    
}