package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.ZoomImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class AdaptadorAutores extends RecyclerView.Adapter<AdaptadorAutores.ViewHolder> {

	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{

		private TextView tvNombre,tvApellidos ,tvtemplanza;
		private ImageView imageView;

		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			tvNombre = itemView.findViewById(R.id.tvnombre);
			tvApellidos = itemView.findViewById(R.id.tvapellido);
			tvtemplanza = itemView.findViewById(R.id.tvtemplanza);
			imageView = itemView.findViewById(R.id.imgfoto);



		}

	}

	public List<Autor> autorList;
	public Context context;


	public AdaptadorAutores(List<Autor> autorList, Context context)
	{
		this.autorList = autorList;
		this.context = context;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_autor,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		//viewHolder.nombre.setText(doctoresLista.get(i).getAut_nombre());
		
		viewHolder.tvNombre.setText(autorList.get(i).getAut_nombre());
		viewHolder.tvApellidos.setText(autorList.get(i).getAut_apellidos());
		viewHolder.tvtemplanza.setText(autorList.get(i).getAut_templanza());
		try
		{
			Glide.with(context).load(autorList.get(i).getAut_foto()).placeholder(R.drawable.cargando).apply(RequestOptions.centerCropTransform()).into(viewHolder.imageView);
		}catch (Exception e)
		{
			System.out.println("error "+e.getLocalizedMessage());
		}

		viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				context.startActivity(new Intent(context, ZoomImageView.class).putExtra("autor",autorList.get(i)));
			}
		});
	}
	
	@Override
	public int getItemCount() {
		return autorList.size();
	}
	
}
