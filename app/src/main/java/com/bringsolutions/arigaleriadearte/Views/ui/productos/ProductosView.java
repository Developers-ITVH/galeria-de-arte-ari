package com.bringsolutions.arigaleriadearte.Views.ui.productos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Producto;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Productos;
import com.bringsolutions.arigaleriadearte.Presenters.ProductosPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorObras;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdaptadorProductos;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class ProductosView extends Fragment implements Productos.View
{

    RecyclerView recyclerView;
    AdaptadorProductos adaptadorProductos;
    View view;
    Productos.Presenter presenter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_productos_view, container, false);

        recyclerView = view.findViewById(R.id.recyclerproductos);
        presenter = new ProductosPresenter(this);

        presenter.obtenerProductos();




        return view;
    }

    @Override
    public void cargarProductos(List<Producto> productos)
    {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        AdaptadorProductos adaptadorProductos;
        adaptadorProductos = new AdaptadorProductos(productos, getContext());
        recyclerView.setAdapter(adaptadorProductos);
    }

    @Override
    public void error(Respuesta respuesta)
    {
        Toasty.error(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void cargando(Boolean aBoolean)
    {
        if (aBoolean)
        {
            recyclerView.setVisibility(View.GONE);
            view.findViewById(R.id.lottieproductos).setVisibility(View.VISIBLE);
        }else
            {
                recyclerView.setVisibility(View.VISIBLE);
                view.findViewById(R.id.lottieproductos).setVisibility(View.GONE);
            }
    }
}

