package com.bringsolutions.arigaleriadearte.Views.ui.noticias_y_eventos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Evento;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Noticia;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.NoticiasyEventos;
import com.bringsolutions.arigaleriadearte.Presenters.NoticiasyEventosPresenter;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdapterEventos;
import com.bringsolutions.arigaleriadearte.Views.Adaptadores.AdapterNoticias;

import java.util.List;

import es.dmoral.toasty.Toasty;


public class NoticiasEventosView extends Fragment implements NoticiasyEventos.View
{
    
    RecyclerView recyclerView;
    Spinner spinner;
    NoticiasyEventos.Presenter presenter;

    View view;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

      view= inflater.inflate(R.layout.fragment_noticias_eventos, container, false);
      
      inicializarElementos();
      poblarRecycler();
      clickSpinner();
      //Presenter


        return view;
    }

    private void clickSpinner()
    {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position){
                    case 0://Noticias
                        presenter.cargarNoticias();

                        break;
                    case 1://Eventos
                        presenter.cargarEventos();

                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void poblarRecycler() {


        presenter.cargarNoticias();
    
    }
    
    private void inicializarElementos() {
        
        recyclerView = view.findViewById(R.id.recyclerNoticias);
        presenter = new NoticiasyEventosPresenter(this);
        spinner = view.findViewById(R.id.spTipoNoticias);
        
    }
    
    @Override
    public void cargarNoticias(List<Noticia> noticias)
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterNoticias adapter;
        adapter = new AdapterNoticias(noticias, getContext());
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void cargarEventos(List<Evento> eventos)
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterEventos adapter;
        adapter = new AdapterEventos(eventos, getContext());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void error(Respuesta respuesta)
    {
        recyclerView.setVisibility(View.GONE);
        Toasty.error(getContext(), respuesta.getRespuesta(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void cargando(boolean b)
    {
        if (b)
        {
            recyclerView.setVisibility(View.GONE);
            view.findViewById(R.id.lottienoticias).setVisibility(View.VISIBLE);
        }else
            {
                recyclerView.setVisibility(View.VISIBLE);
                view.findViewById(R.id.lottienoticias).setVisibility(View.GONE);
            }
    }
}