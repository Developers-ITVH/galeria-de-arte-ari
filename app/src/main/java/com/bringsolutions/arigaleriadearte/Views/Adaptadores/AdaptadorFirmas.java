package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Firma;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.ZoomImageView;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdaptadorFirmas extends RecyclerView.Adapter<AdaptadorFirmas.ViewHolder> {

	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{

		private TextView tvNombre,tvLugar,tvFecha;
		private ImageView imageView;

		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			tvNombre = itemView.findViewById(R.id.tvnombre);
			tvLugar = itemView.findViewById(R.id.tvlugarnacimiento);
			tvFecha = itemView.findViewById(R.id.tvfecha);
			imageView = itemView.findViewById(R.id.imageView7);


		}

	}

	public List<Firma> firmaList;
	public Context context;

	public AdaptadorFirmas(List<Firma> firmaList, Context context)
	{
		this.firmaList = firmaList;
		this.context = context;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_firma,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		//viewHolder.nombre.setText(doctoresLista.get(i).getAut_nombre());
		
		viewHolder.tvNombre.setText(firmaList.get(i).getFir_nombre_autor());
		viewHolder.tvFecha.setText(firmaList.get(i).getFir_fecha());
		viewHolder.tvLugar.setText(firmaList.get(i).getFir_lugar_nacimiento());
		Glide.with(context).load(firmaList.get(i).getFir_foto()).placeholder(R.drawable.cargando).into(viewHolder.imageView);
		viewHolder.imageView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				context.startActivity(new Intent(context, ZoomImageView.class).putExtra("firma",firmaList.get(i)));
			}
		});
		
	}
	
	@Override
	public int getItemCount() {
		return firmaList.size();
	}
	
}
