package com.bringsolutions.arigaleriadearte.Views.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.R;
import com.bringsolutions.arigaleriadearte.Views.ZoomImageView;
import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.util.List;

public class AdaptadorEsculturas extends RecyclerView.Adapter<AdaptadorEsculturas.ViewHolder> {
	
	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		
		private TextView tvTitulo, tvAutor, tvDescripcion, tvPrecio, tvAnio, tvAncho, tvAlto, tvTecnica, tvEstado;
		private ImageView imageView;
		
		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			tvTitulo = itemView.findViewById(R.id.tvTitulo);
			tvAutor = itemView.findViewById(R.id.tvAutor);
			tvDescripcion = itemView.findViewById(R.id.tvDescripcion);
			tvPrecio = itemView.findViewById(R.id.tvPrecio);
			tvAnio = itemView.findViewById(R.id.tvAnio);
			tvAncho = itemView.findViewById(R.id.tvAncho);
			tvAlto = itemView.findViewById(R.id.tvAlto);
			tvTecnica = itemView.findViewById(R.id.tvTecnica);
			tvEstado = itemView.findViewById(R.id.tvEstado);
			imageView = itemView.findViewById(R.id.imageView3);
		}
		
	}
	
		public List<Escultura> esculturaList;
	public Context context;
	
	public AdaptadorEsculturas(List<Escultura> esculturaList, Context context) {
		this.esculturaList = esculturaList;
		this.context = context;
	}
	
	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_obra_escultura,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		DecimalFormat formateador = new DecimalFormat("###,###.##");

		viewHolder.tvTitulo.setText(esculturaList.get(i).getEsc_nombre());
		viewHolder.tvAutor.setText("Autor : "+esculturaList.get(i).getEsc_clave_autor());
		viewHolder.tvDescripcion.setText(esculturaList.get(i).getEsc_descripcion());
		viewHolder.tvPrecio.setText("Precio : $ "+formateador.format(Integer.parseInt(esculturaList.get(i).getEsc_precio())));
		viewHolder.tvAnio.setText("Año : "+esculturaList.get(i).getEsc_anio());
		viewHolder.tvAncho.setText("Ancho : "+esculturaList.get(i).getEsc_ancho());
		viewHolder.tvAlto.setText("Alto : "+esculturaList.get(i).getEsc_alto());
		viewHolder.tvTecnica.setText("Material : "+esculturaList.get(i).getEsc_material());

		viewHolder.tvEstado.setText(esculturaList.get(i).getEsc_estado());
		Glide.with(context).load(esculturaList.get(i).getEsc_foto()).placeholder(R.drawable.cargando).into(viewHolder.imageView);

		viewHolder.imageView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				context.startActivity(new Intent(context, ZoomImageView.class).putExtra("escultura",esculturaList.get(i)));
			}
		});

		
	}
	
	@Override
	public int getItemCount() {
		return esculturaList.size();
	}
	
}
