package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Comentario;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

import java.util.List;

public interface Comentarios
{
    interface View
    {
        void error(Respuesta respuesta);
        void cargarComentarios(List<Comentario> comentarios);
        void resultcomentario(Respuesta respuesta);
        void cargando(boolean b);
    }

    interface Presenter
    {
        void obtenerComentarios();
        void sendComentario(Comentario comentario);
        void error(Respuesta respuesta);
        void resultComentario(Respuesta respuesta);
        void resultComentarios(List<Comentario>  comentarios);
    }

    interface Interactor
    {
        void getComentarios();
        void putComentario(Comentario comentario);
    }
}
