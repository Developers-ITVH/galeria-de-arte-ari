package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

import java.util.List;

public interface Autores
{
    interface View
    {
        void cargarAutores(List<Autor> autors);
        void error(Respuesta respuesta);
        void cargando(Boolean aBoolean);
    }


    interface Presenter
    {
        void cargarAutores();
        void resultAutores(List<Autor> autors);
        void error(Respuesta respuesta);
    }

    interface Interactor
    {
        void cargarAutores();
    }
}
