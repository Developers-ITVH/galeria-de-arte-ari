package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

public interface Identificar
{
    interface Interactor
    {
        void consultarObra(Obra obra);
        void consultarEscultura(Escultura escultura);
    }

    interface Presentador
    {
        void consultObra(Obra obra);
        void resultObra(Obra obra);
        void consultEscultura(Escultura escultura);
        void resultEscultura(Escultura  escultura);
        void errorResult(Respuesta respuesta);
    }

    interface View
    {
        void resultObra (Obra obra);
        void resultEscultura(Escultura escultura);
        void errorResult(Respuesta respuesta);
    }


}
