package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;

public interface Login
{
    interface Interactor
    {
        void Login(Usuario usuario);
    }

    interface Presenter
    {
        void Login(Usuario usuario);
        void ResultError(Respuesta Respuesta);
        void ResultPresenter(Usuario  usuario);
    }

    interface View
    {
        void SetHome(Usuario usuario);
        void Cargando (boolean b);
        void ShowError(Respuesta respuesta);
    }
}
