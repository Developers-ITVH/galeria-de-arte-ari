package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Producto;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

import java.util.List;

public interface Productos
{
    interface View
    {
        void cargarProductos(List<Producto> productos);
        void error(Respuesta respuesta);
        void cargando(Boolean aBoolean);
    }

    interface Presenter
    {
        void obtenerProductos();
        void error(Respuesta respuesta);
        void cargarProductos(List<Producto> productos);
    }

    interface Interactor
    {
        void getProductos();
    }
}
