package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;

public interface ActualizarUsuario
{
    interface View
    {
        void resultUpdate(Respuesta respuesta);
        void Cargando (boolean b);
        void ShowError(Respuesta respuesta);
    }

    interface Presenter
    {
        void updateUsuario(Usuario usuario);
        void resultUsuario(Respuesta respuesta);
        void errorUsuario(Respuesta respuesta);
    }

    interface Interactor
    {
        void updateUsuario(Usuario usuario);
    }
}
