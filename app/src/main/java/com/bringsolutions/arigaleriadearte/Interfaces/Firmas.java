package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Firma;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

import java.util.List;

public interface Firmas
{
    interface View
    {
        void resultFirmas(List<Firma> firmas);
        void error(Respuesta respuesta);
        void cargando(Boolean aBoolean);
    }

    interface Presenter
    {
        void obtenerFirmas();
        void resultFirmas(List<Firma> firmas);
        void error(Respuesta respuesta);
    }

    interface Interactor
    {
        void obtenerFirmas();
    }
}
