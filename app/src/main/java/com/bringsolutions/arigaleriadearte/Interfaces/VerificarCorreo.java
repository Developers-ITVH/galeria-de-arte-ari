package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Code;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;

public interface VerificarCorreo
{
    interface View
    {
        void resultCorreo(Respuesta respuesta);
        void error(Respuesta respuesta);
        void cargando(Boolean aBoolean);
    }

    interface Presenter
    {
        void verificarCorreo(Usuario usuario);
        void resultCorreo(Respuesta respuesta);
        void error(Respuesta respuesta);
    }

    interface Interactor
    {
        void verificarCorreo(Usuario usuario);
    }


    interface CodigoVerificacion
    {
        interface View
        {
            void resultCode(Respuesta respuesta);
            void error(Respuesta respuesta);
            void cargando(Boolean aBoolean);
        }

        interface Presenter
        {
            void verificarCode(Code code);
            void resultCode(Respuesta respuesta);
            void error(Respuesta respuesta);
        }

        interface Interactor
        {
            void veriicarCode(Code code);
        }
    }
}
