package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Evento;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Noticia;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

import java.util.List;

public interface NoticiasyEventos
{
    interface View
    {
        void cargarNoticias(List<Noticia> noticias);
        void cargarEventos(List<Evento> eventos);
        void error(Respuesta respuesta);
        void cargando(boolean b);
    }

    interface Presenter
    {
        void cargarNoticias();
        void cargarEventos();
        void error(Respuesta respuesta);
        void resultNoticias(List<Noticia> noticias);
        void resultEventos(List<Evento> eventos);
    }

    interface Interactor
    {
        void cargarNoticias();
        void cargarEventos();
    }
}
