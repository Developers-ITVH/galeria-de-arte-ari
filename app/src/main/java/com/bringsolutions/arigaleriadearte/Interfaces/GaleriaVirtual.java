package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Galeria_Virtual;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

import java.util.List;

public interface GaleriaVirtual
{
    interface View
    {
        void resultGaleriaVirtual(List<Galeria_Virtual> galeria_virtuals);
        void shoError(Respuesta respuesta);
        void cargando(boolean b);
    }

    interface Presenter

    {
        void getGaleriaVirtual();
        void resultGaleriaVirtual(List<Galeria_Virtual> galeria_virtuals);
        void errorResult(Respuesta respuesta);
    }

    interface Interactor
    {
        void getGaleriaVirtual();
    }
}
