package com.bringsolutions.arigaleriadearte.Interfaces;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;

import java.util.List;

public interface Obras_Esculturas
{
     interface View
    {
        void cargandoDatos (Boolean aBoolean);
        void error(Respuesta respuesta);
        void cargarObras(List<Obra> obras);
        void cargarEscultura(List<Escultura> esculturas);
    }

     interface Presenter
    {
        void obtenerObras();
        void obtenerEsculturas();
        void error (Respuesta respuesta);
        void resultObras(List<Obra> obras);
        void resultEsculturas(List<Escultura> esculturas);
    }

     interface Interactor

    {
        void consultarObras();
        void consultarEsculturas();
    }
}
