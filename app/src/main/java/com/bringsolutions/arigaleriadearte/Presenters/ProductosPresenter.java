package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Producto;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.ProductosInteractor;
import com.bringsolutions.arigaleriadearte.Interfaces.Productos;
import com.bringsolutions.arigaleriadearte.Views.ui.productos.ProductosView;

import java.util.List;

public class ProductosPresenter implements Productos.Presenter
{
    Productos.Interactor interactor;
    ProductosView view;


    public ProductosPresenter(ProductosView view)
    {
        this.view = view;
        this.interactor = new ProductosInteractor(this);
    }

    @Override
    public void obtenerProductos()
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.getProductos();
        }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.cargando(false);
            view.error(respuesta);
        }
    }

    @Override
    public void cargarProductos(List<Producto> productos)
    {
        if (view!=null)
        {
            view.cargarProductos(productos);
            view.cargando(false);

        }
    }
}
