package com.bringsolutions.arigaleriadearte.Presenters;

import android.util.Log;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.identificarInteractor;
import com.bringsolutions.arigaleriadearte.Interfaces.Identificar;
import com.bringsolutions.arigaleriadearte.Views.ui.identificar.IdentificarView;

public class identificarPresenter implements Identificar.Presentador
{
    Identificar.Interactor interactor;
    IdentificarView view;

    public identificarPresenter( IdentificarView view)
    {
        this.interactor = new identificarInteractor(this);
        this.view = view;
    }

    @Override
    public void consultObra(Obra obra)
    {
        if (obra.getObr_clave().equals(""))
        {
            errorResult(new Respuesta("Codigo de Obra - Escultura inexistente"));
        }else
            {
                interactor.consultarObra(obra);
            }
    }

    @Override
    public void resultObra(Obra obra)
    {
        if (view!=null)
        {
            view.resultObra(obra);

        }
    }

    @Override
    public void consultEscultura(Escultura escultura)
    {
        interactor.consultarEscultura(escultura);
    }

    @Override
    public void resultEscultura(Escultura escultura)
    {
        if (view!=null)
        {
            view.resultEscultura(escultura);
        }
    }

    @Override
    public void errorResult(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.errorResult(respuesta);
        }
    }
}
