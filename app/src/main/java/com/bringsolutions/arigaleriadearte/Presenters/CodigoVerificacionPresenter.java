package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.CodigoVerificacionInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Code;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.VerificarCorreo;
import com.bringsolutions.arigaleriadearte.Views.CodigoVerificacionView;

public class CodigoVerificacionPresenter implements VerificarCorreo.CodigoVerificacion.Presenter
{
    CodigoVerificacionView view;
    CodigoVerificacionInteractor interactor;

    public CodigoVerificacionPresenter(CodigoVerificacionView view)
    {
        this.view = view;
        this.interactor = new CodigoVerificacionInteractor(this);
    }

    @Override
    public void verificarCode(Code code)
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.veriicarCode(code);
        }
    }

    @Override
    public void resultCode(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.cargando(false);
            view.resultCode(respuesta);
        }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.cargando(false);
            view.error(respuesta);
        }
    }
}
