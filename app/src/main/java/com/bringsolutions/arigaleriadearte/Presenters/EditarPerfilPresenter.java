package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.EditarPerfilInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.ActualizarUsuario;
import com.bringsolutions.arigaleriadearte.Views.EditarPerfilView;

public class EditarPerfilPresenter implements ActualizarUsuario.Presenter
{
    EditarPerfilInteractor interactor;
    EditarPerfilView view;

    public EditarPerfilPresenter(EditarPerfilView view)
    {
        this.view = view;
        this.interactor = new EditarPerfilInteractor(this);
    }

    @Override
    public void updateUsuario(Usuario usuario)
    {
        if (view!=null)
        {
            view.Cargando(true);
            interactor.updateUsuario(usuario);
        }
    }

    @Override
    public void resultUsuario(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.Cargando(false);
            view.resultUpdate(respuesta);
        }
    }

    @Override
    public void errorUsuario(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.Cargando(false);
            view.resultUpdate(respuesta);
        }
    }
}
