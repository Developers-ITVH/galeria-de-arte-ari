package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interactor.VerificarCorreoInteractor;
import com.bringsolutions.arigaleriadearte.Interfaces.VerificarCorreo;
import com.bringsolutions.arigaleriadearte.Views.VerificarCorreoView;

public class VerificarCorreoPresenter implements VerificarCorreo.Presenter
{
    VerificarCorreoInteractor interactor;
    VerificarCorreoView view;

    public VerificarCorreoPresenter(VerificarCorreoView view)
    {
        this.view = view;
        this.interactor = new VerificarCorreoInteractor(this);
    }

    @Override
    public void verificarCorreo(Usuario usuario)
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.verificarCorreo(usuario);
        }
    }

    @Override
    public void resultCorreo(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.cargando(false);
            view.resultCorreo(respuesta);
        }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        if(view!=null)
        {
            view.cargando(false);
            view.error(respuesta);
        }
    }
}
