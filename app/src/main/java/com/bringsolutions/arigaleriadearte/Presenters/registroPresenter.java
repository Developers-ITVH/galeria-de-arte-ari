package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interactor.RegistroInteractor;
import com.bringsolutions.arigaleriadearte.Interfaces.Registro;
import com.bringsolutions.arigaleriadearte.Views.RegistroView;

public class registroPresenter implements Registro.Presenter
{
    RegistroView view;
    Registro.Interactor interactor;

    public registroPresenter(RegistroView view)
    {
        this.view = view;
        this.interactor = new RegistroInteractor(this);
    }

    @Override
    public void Registro(Usuario usuario)
    {
        /*if (usuario.getUsu_nom().isEmpty()||usuario.getUsu_ape_p().isEmpty()||usuario.getUsu_ape_m().isEmpty()||usuario.getUsu_tel().isEmpty()||usuario.getUsu_ema().isEmpty()||usuario.getUsu_pas().isEmpty())
        {
            ResultError(new Respuesta("favor de rellenar todos los campos"));
        }else
            {

            }*/

        interactor.Registro(usuario);
    }

    @Override
    public void ResultError(Respuesta Respuesta)
    {
        view.ShowError(Respuesta);
    }

    @Override
    public void ResultPresenter(Usuario usuario)
    {
        view.SetHome(usuario);
    }
}
