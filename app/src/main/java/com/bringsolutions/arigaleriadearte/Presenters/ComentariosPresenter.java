package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.ComentariosInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Comentario;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Comentarios;
import com.bringsolutions.arigaleriadearte.Views.ui.comentarios.ComentariosView;

import java.util.List;

public class ComentariosPresenter implements Comentarios.Presenter
{
    ComentariosView view;
    Comentarios.Interactor interactor;

    public ComentariosPresenter(ComentariosView view)
    {
        this.view = view;
        this.interactor = new ComentariosInteractor(this);
    }

    @Override
    public void obtenerComentarios()
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.getComentarios();
        }
    }

    @Override
    public void sendComentario(Comentario comentario)
    {
        if (view!=null)
        {
            interactor.putComentario(comentario);
        }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.error(respuesta);
        }
    }

    @Override
    public void resultComentario(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.resultcomentario(respuesta);
        }
    }

    @Override
    public void resultComentarios(List<Comentario> comentarios)
    {
        if (view!=null)
        {
            if (comentarios.size()==0)
            {
                view.error(new Respuesta("Sin Datos"));
            }else
                {
                    view.cargarComentarios(comentarios);
                }

            view.cargando(false);


        }
    }
}
