package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Evento;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Noticia;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.NoticiasyEventosInteractor;
import com.bringsolutions.arigaleriadearte.Interfaces.NoticiasyEventos;
import com.bringsolutions.arigaleriadearte.Views.ui.noticias_y_eventos.NoticiasEventosView;

import java.util.List;

public class NoticiasyEventosPresenter implements NoticiasyEventos.Presenter
{
    NoticiasEventosView view;
    NoticiasyEventos.Interactor interactor;

    public NoticiasyEventosPresenter(NoticiasEventosView view)
    {
        this.view = view;
        interactor = new NoticiasyEventosInteractor(this);
    }

    @Override
    public void cargarNoticias()
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.cargarNoticias();

        }
    }

    @Override
    public void cargarEventos()
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.cargarEventos();
        }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        view.error(respuesta);
    }

    @Override
    public void resultNoticias(List<Noticia> noticias)
    {
       if (view!=null)
       {
           if (noticias.size()==0)
           {
               view.error(new Respuesta("Sin Datos !!"));
               view.cargando(true);
           }else
               {
                   view.cargarNoticias(noticias);
                   view.cargando(false);

               }

       }
    }

    @Override
    public void resultEventos(List<Evento> eventos)
    {
        if (eventos.size()==0)
        {
            view.error(new Respuesta("Sin Datos !!"));
            view.cargando(true);
        }else
            {
                view.cargarEventos(eventos);
                view.cargando(false);

            }
    }
}
