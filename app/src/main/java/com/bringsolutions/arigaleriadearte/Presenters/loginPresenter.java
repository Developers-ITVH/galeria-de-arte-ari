package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.loginInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Usuario;
import com.bringsolutions.arigaleriadearte.Interfaces.Login;
import com.bringsolutions.arigaleriadearte.Views.LoginView;

public class loginPresenter implements Login.Presenter
{

    LoginView view;
    Login.Interactor interactor;

    public loginPresenter(LoginView view)
    {
        this.view = view;
        this.interactor = new loginInteractor(this);
    }

    @Override
    public void Login(Usuario usuario)
    {
        view.Cargando(true);
     /*   if (usuario.getUsu_tel().equals("")||usuario.getUsu_pas().equals(""))
        {
            view.ShowError(new Respuesta("Uno de los campos esta vacio"));
            view.Cargando(false);
        }else
            {
                interactor.Login(usuario);
            }*/

     interactor.Login(usuario);

    }

    @Override
    public void ResultError(Respuesta Respuesta)
    {
        view.Cargando(false);
        view.ShowError(Respuesta);
    }


    @Override
    public void ResultPresenter(Usuario usuario)
    {
        view.Cargando(false);
        view.SetHome(usuario);
    }
}
