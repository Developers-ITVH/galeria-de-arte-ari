package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.AutoresInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.FirmasInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Autor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Autores;
import com.bringsolutions.arigaleriadearte.Interfaces.Firmas;
import com.bringsolutions.arigaleriadearte.Views.ui.autores.AutoresView;

import java.util.List;

public class AutoresPresenter implements Autores.Presenter
{
    AutoresView view ;
    Autores.Interactor interactor;

    public AutoresPresenter(AutoresView view)
    {
        this.view = view;
        this.interactor = new AutoresInteractor(this);
    }

    @Override
    public void cargarAutores()
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.cargarAutores();
        }
    }

    @Override
    public void resultAutores(List<Autor> autors)
    {
        if (view!=null)
        {
            view.cargando(false);
            view.cargarAutores(autors);
        }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.cargando(false);
            view.error(respuesta);
        }
    }
}
