package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.Models.Escultura;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Obra;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interactor.ObrasEsculturasInteractor;
import com.bringsolutions.arigaleriadearte.Interfaces.Obras_Esculturas;
import com.bringsolutions.arigaleriadearte.Views.ui.obras_esculturas.ObrasEsculturasView;

import java.util.List;

public class ObrasEsculturasPresenter implements Obras_Esculturas.Presenter
{
    ObrasEsculturasView view;
    Obras_Esculturas.Interactor interactor;

    public ObrasEsculturasPresenter(ObrasEsculturasView view)
    {
        this.view = view;
        this.interactor = new ObrasEsculturasInteractor(this);
    }



    @Override
    public void obtenerObras()
    {
        view.cargandoDatos(true);
        interactor.consultarObras();
    }

    @Override
    public void obtenerEsculturas()
    {
        view.cargandoDatos(true);
        interactor.consultarEsculturas();
    }

    @Override
    public void error(Respuesta respuesta)
    {
        view.error(respuesta);
    }

    @Override
    public void resultObras(List<Obra> obras)
    {
        if (view!=null)
        {
            if (obras.size()==0)
            {
                view.error(new Respuesta("Sin Datos"));
            }else
                {
                    view.cargarObras(obras);
                }
            view.cargandoDatos(false);

        }
    }

    @Override
    public void resultEsculturas(List<Escultura> esculturas)
    {
        if (view!=null)
        {
            if (esculturas.size()==0)
            {
                view.error(new Respuesta("Sin Datos"));
            }else
                {
                    view.cargandoDatos(false);
                }
            view.cargarEscultura(esculturas);
        }
    }


}
