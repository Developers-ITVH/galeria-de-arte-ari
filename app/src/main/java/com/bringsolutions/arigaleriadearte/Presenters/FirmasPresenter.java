package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.FirmasInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Firma;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.Firmas;
import com.bringsolutions.arigaleriadearte.Views.ui.firmas.FirmasView;

import java.util.List;

public class FirmasPresenter implements Firmas.Presenter
{

    Firmas.Interactor interactor;
    FirmasView view;


    public FirmasPresenter(FirmasView view)
    {
        this.view = view;
        this.interactor = new FirmasInteractor(this);
    }

    @Override
    public void obtenerFirmas()
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.obtenerFirmas();
        }
    }

    @Override
    public void resultFirmas(List<Firma> firmas)
    {
        if (view!=null)
        {
            if (firmas.size()==0)
            {
                view.error(new Respuesta("Sin datos"));
            }else
                {
                    view.resultFirmas(firmas);
                }
            view.cargando(false);


        }
    }

    @Override
    public void error(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.cargando(false);
            view.error(respuesta);
        }
    }
}
