package com.bringsolutions.arigaleriadearte.Presenters;

import com.bringsolutions.arigaleriadearte.Interactor.GaleriaVirtualInteractor;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Galeria_Virtual;
import com.bringsolutions.arigaleriadearte.Interactor.Models.Respuesta;
import com.bringsolutions.arigaleriadearte.Interfaces.GaleriaVirtual;
import com.bringsolutions.arigaleriadearte.Views.ui.galeria_virtual.GaleriaVirtualView;

import java.util.List;

public class GaleriaVirtualPresenter implements GaleriaVirtual.Presenter
{
    GaleriaVirtualView view;
    GaleriaVirtualInteractor interactor;

    public GaleriaVirtualPresenter(GaleriaVirtualView view)
    {
        this.view = view;
        this.interactor = new GaleriaVirtualInteractor(this);
    }

    @Override
    public void getGaleriaVirtual()
    {
        if (view!=null)
        {
            view.cargando(true);
            interactor.getGaleriaVirtual();
        }
    }

    @Override
    public void resultGaleriaVirtual(List<Galeria_Virtual> galeria_virtuals)


    {

        if (view!=null)
        {

            view.resultGaleriaVirtual(galeria_virtuals);
            view.cargando(false);
        }
    }

    @Override
    public void errorResult(Respuesta respuesta)
    {
        if (view!=null)
        {
            view.shoError(respuesta);
            view.cargando(false);
        }
    }
}
